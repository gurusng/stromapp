import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  showCode:boolean = false;
  code:string;
  password:string;
  confirmPassword:string;
  email:string;
  loading:boolean = false;

  constructor(public navCtrl: NavController,
              public userProvider : UserProvider,
              public viewCtrl: ViewController,
              public navParams: NavParams) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ForgotPasswordPage');
  }

  resetPassword(){
    let data = {
      'email' : this.email
    };

    this.userProvider.resetPassword(data).subscribe((success:any)=>{

      this.loading = false;
      if(success.message == "Successful"){
        this.showCode = true;

      } else{
          this.userProvider.showAlert("Error",success.message);
      }

    },(error)=>{
        this.loading = false;
        //console.log(JSON.stringify(error))
    });

  }

    resetPasswordCode(){
        let data = {
            'code' : this.code,
            'password' : this.password,
            'confirmPassword' : this.confirmPassword
        };

        this.loading = true;
        this.userProvider.resetPasswordCode(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.showCode = false;
                this.viewCtrl.dismiss();
                this.userProvider.showToast("Password reset successful.");

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }



    dismiss(){
    this.viewCtrl.dismiss();
  }
}
