var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ForgotPasswordPage = /** @class */ (function () {
    function ForgotPasswordPage(navCtrl, userProvider, viewCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.showCode = false;
        this.loading = false;
    }
    ForgotPasswordPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad ForgotPasswordPage');
    };
    ForgotPasswordPage.prototype.resetPassword = function () {
        var _this = this;
        var data = {
            'email': this.email
        };
        this.userProvider.resetPassword(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.showCode = true;
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    ForgotPasswordPage.prototype.resetPasswordCode = function () {
        var _this = this;
        var data = {
            'code': this.code,
            'password': this.password,
            'confirmPassword': this.confirmPassword
        };
        this.loading = true;
        this.userProvider.resetPasswordCode(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.showCode = false;
                _this.viewCtrl.dismiss();
                _this.userProvider.showToast("Password reset successful.");
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    ForgotPasswordPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ForgotPasswordPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-forgot-password',
            templateUrl: 'forgot-password.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            ViewController,
            NavParams])
    ], ForgotPasswordPage);
    return ForgotPasswordPage;
}());
export { ForgotPasswordPage };
//# sourceMappingURL=forgot-password.js.map