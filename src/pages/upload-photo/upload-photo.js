var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { UserProvider } from "../../providers/user/user";
/**
 * Generated class for the UploadPhotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UploadPhotoPage = /** @class */ (function () {
    function UploadPhotoPage(navCtrl, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.navParams = navParams;
    }
    UploadPhotoPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad UploadPhotoPage');
    };
    UploadPhotoPage.prototype.skip = function () {
        this.navCtrl.push(TabsPage);
    };
    UploadPhotoPage.prototype.uploadPhoto = function () {
    };
    UploadPhotoPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-upload-photo',
            templateUrl: 'upload-photo.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            NavParams])
    ], UploadPhotoPage);
    return UploadPhotoPage;
}());
export { UploadPhotoPage };
//# sourceMappingURL=upload-photo.js.map