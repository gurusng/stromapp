var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProfilePage } from "../user-profile/user-profile";
import { UserProvider } from "../../providers/user/user";
import { AddCardPage } from "../add-card/add-card";
var FundPage = /** @class */ (function () {
    function FundPage(navCtrl, userProvider, app, alertCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.loading = false;
        this.fundLoading = false;
        this.points = null;
    }
    FundPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad FundPage');
    };
    FundPage.prototype.userDetail = function () {
        this.app.getRootNav().setRoot(UserProfilePage);
    };
    FundPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    FundPage.prototype.goToAddCard = function () {
        this.navCtrl.push(AddCardPage);
    };
    FundPage.prototype.confirmCardDelete = function (card) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Confirmation',
            message: 'Are you sure you want to delete this card?',
            buttons: [
                {
                    text: 'No',
                    handler: function () {
                        //console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.deleteCard(card);
                        //console.log('Agree clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    FundPage.prototype.deleteCard = function (card) {
        var _this = this;
        var data = {
            'cardid': card.cardid
        };
        this.loading = true;
        this.userProvider.deleteCard(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Success", "Card Deleted.");
                _this.userProvider.userUpdatedDetails();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    FundPage.prototype.selectCard = function (card) {
        this.cardid = card.cardid;
    };
    FundPage.prototype.fund = function () {
        var _this = this;
        if (this.points <= 0) {
            this.userProvider.showAlert("Error", "Please enter number of points or amount");
            return;
        }
        if (this.cardid == null || this.cardid <= 0) {
            this.userProvider.showAlert("Error", "Please select a card");
            return;
        }
        var data = {
            'uid': this.userProvider.user.uid,
            'cardid': this.cardid,
            'amount': this.points
        };
        this.loading = true;
        this.userProvider.pay(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.cardid = null;
                _this.points = null;
                _this.userProvider.showAlert("Successful", "You account has been funded.");
                _this.userProvider.userUpdatedDetails();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    FundPage.prototype.fundNewCard = function () {
        if (this.points <= 0) {
            this.userProvider.showAlert("Error", "Please enter an amount over 20 naira");
            return;
        }
        var data = {
            'uid': this.userProvider.user.uid,
            'amount': this.points
        };
        this.fundLoading = true;
        var self = this;
        var quidpayscript = document.createElement('script');
        quidpayscript.setAttribute('src', 'https://api.ravepay.co/flwv3-pug/getpaidx/api/quidpay-inline.js');
        document.head.appendChild(quidpayscript);
        quidpayscript.onload = function () {
            self.userProvider.webPay(data).subscribe(function (success) {
                self.fundLoading = false;
                if (success.message == "Successful") {
                    //console.log(success.data.publicKey);
                    var x = getpaidSetup({
                        PBFPubKey: success.data.publicKey,
                        customer_email: self.userProvider.user.email,
                        amount: success.data.amount,
                        customer_phone: self.userProvider.user.phone,
                        currency: "NGN",
                        txref: success.data.reference,
                        meta: [{
                                metaname: "userid",
                                metavalue: self.userProvider.user.uid
                            }],
                        onclose: function () { },
                        callback: function (response) {
                            // var txref = response.tx.txRef; // collect txRef returned and pass to a 					server page to complete status check.
                            //console.log("This is the response returned after a charge", response);
                            if (response.tx.chargeResponseCode == "00" ||
                                response.tx.chargeResponseCode == "0") {
                                self.userProvider.creditPayments({}).subscribe(function (success) {
                                    //console.log(success);
                                }, function (error) {
                                    //console.log(JSON.stringify(error))
                                });
                                // redirect to a success page
                            }
                            else {
                                // redirect to a failure page.
                            }
                            x.close(); // use this to close the modal immediately after payment.
                        }
                    });
                }
                else {
                    self.userProvider.showAlert("Error", success.message);
                }
            }, function (error) {
                self.loading = false;
                //console.log(JSON.stringify(error))
            });
        };
    };
    FundPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-fund',
            templateUrl: 'fund.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            App,
            AlertController,
            NavParams])
    ], FundPage);
    return FundPage;
}());
export { FundPage };
//# sourceMappingURL=fund.js.map