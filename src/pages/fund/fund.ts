import { Component } from '@angular/core';
import {AlertController, App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserProfilePage} from "../user-profile/user-profile";
import {UserProvider} from "../../providers/user/user";
import {AddCardPage} from "../add-card/add-card";

/**
 * Generated class for the FundPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var getpaidSetup;

@IonicPage()
@Component({
  selector: 'page-fund',
  templateUrl: 'fund.html',
})

export class FundPage {
    loading:boolean = false;
    fundLoading:boolean = false;
    points : number = null;
    cardid:number;

    constructor(public navCtrl: NavController,
                public userProvider: UserProvider,
                private app : App,
                public alertCtrl: AlertController,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad FundPage');
    }

    userDetail(){
        this.app.getRootNav().setRoot(UserProfilePage);
    }
    goBack() {
        this.navCtrl.pop();
    }

    goToAddCard(){
        this.navCtrl.push(AddCardPage);
    }

    confirmCardDelete(card) {
        const confirm = this.alertCtrl.create({
            title: 'Confirmation',
            message: 'Are you sure you want to delete this card?',
            buttons: [
                {
                    text: 'No',
                    handler: () => {
                        //console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.deleteCard(card);
                        //console.log('Agree clicked');
                    }
                }
            ]
        });
        confirm.present();
    }

    deleteCard(card){
        let data = {
            'cardid' : card.cardid
        };
        this.loading = true;
        this.userProvider.deleteCard(data).subscribe((success:any)=>{

            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert("Success","Card Deleted.")
                this.userProvider.userUpdatedDetails();

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    selectCard(card){
        this.cardid = card.cardid;
    }

    fund(){
        if(this.points <= 0) {
            this.userProvider.showAlert("Error", "Please enter number of points or amount");
            return;
        }

        if(this.cardid == null || this.cardid <= 0){
            this.userProvider.showAlert("Error","Please select a card");
            return;
        }


        let data = {
            'uid' : this.userProvider.user.uid,
            'cardid' : this.cardid,
            'amount' : this.points
        };

        this.loading = true;
        this.userProvider.pay(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.cardid = null;
                this.points = null;
                this.userProvider.showAlert("Successful", "You account has been funded.");
                this.userProvider.userUpdatedDetails();

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    fundNewCard(){

        if(this.points <= 0 ) {
            this.userProvider.showAlert("Error","Please enter an amount over 20 naira");
            return;
        }

        let data = {
            'uid' : this.userProvider.user.uid,
            'amount' : this.points
        };

        this.fundLoading = true;
        let self = this;

        let quidpayscript:any = document.createElement('script');

        quidpayscript.setAttribute('src',this.userProvider.inlinePaymentUrl);

        document.head.appendChild(quidpayscript);

        quidpayscript.onload = function(){

            self.userProvider.webPay(data).subscribe((success:any)=>{
                self.fundLoading = false;

                if(success.message == "Successful"){

                    //console.log(success.data.publicKey);
                    var x = getpaidSetup({
                        PBFPubKey: success.data.publicKey,
                        customer_email: self.userProvider.user.email,
                        amount: success.data.amount,
                        customer_phone: self.userProvider.user.phone,
                        currency: "NGN",
                        txref: success.data.reference,
                        meta: [{
                            metaname: "userid",
                            metavalue: self.userProvider.user.uid
                        }],
                        onclose: function() {},
                        callback: function(response) {
                            // var txref = response.tx.txRef; // collect txRef returned and pass to a 					server page to complete status check.
                            //console.log("This is the response returned after a charge", response);
                            if (
                                response.tx.chargeResponseCode == "00" ||
                                response.tx.chargeResponseCode == "0"
                            ) {
                                self.userProvider.creditPayments({}).subscribe((success:any)=>{
                                    //console.log(success);
                                },(error)=>{
                                    //console.log(JSON.stringify(error))
                                });

                                // redirect to a success page
                            } else {
                                // redirect to a failure page.
                            }

                            x.close(); // use this to close the modal immediately after payment.
                        }
                    });


                } else{
                    self.userProvider.showAlert("Error",success.message);
                }

            },(error)=>{
                self.loading = false;
                //console.log(JSON.stringify(error))
            });

        }



    }

}
