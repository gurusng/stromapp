var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IndexPage } from "../index";
import { Storage } from "@ionic/storage";
/**
 * Generated class for the LandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LandingPage = /** @class */ (function () {
    function LandingPage(navCtrl, storage, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.slideData = [
            { image: "../assets/imgs/slide1.png" },
            { image: "../assets/imgs/slide1.png" },
            { image: "../assets/imgs/slide1.png" },
            { image: "../assets/imgs/slide1.png" }
        ];
        this.storage.get("showLanding").then(function (data) {
            if (data == false)
                _this.navCtrl.setRoot(IndexPage);
        });
    }
    LandingPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad LandingPage');
    };
    LandingPage.prototype.goToIndex = function () {
        this.storage.set('showLanding', false);
        this.navCtrl.setRoot(IndexPage);
    };
    LandingPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-landing',
            templateUrl: 'landing.html',
        }),
        __metadata("design:paramtypes", [NavController,
            Storage,
            NavParams])
    ], LandingPage);
    return LandingPage;
}());
export { LandingPage };
//# sourceMappingURL=landing.js.map