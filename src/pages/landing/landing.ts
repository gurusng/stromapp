import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {IndexPage} from "../index";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the LandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  slideData = [
    { image: "../assets/imgs/slide1.png" },
    { image: "../assets/imgs/slide1.png" },
    { image: "../assets/imgs/slide1.png" },
    { image: "../assets/imgs/slide1.png" }
  ]

  constructor(public navCtrl: NavController,
              public storage : Storage,
              public navParams: NavParams) {

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LandingPage');
  }

  goToIndex(){

      this.storage.set('showLanding',false);
      this.navCtrl.setRoot(IndexPage)
  }


}
