var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
import { UserProfilePage } from '../user-profile/user-profile';
/**
 * Generated class for the VideoDescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VideoDescriptionPage = /** @class */ (function () {
    function VideoDescriptionPage(navCtrl, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.video = null;
        this.loading = false;
        this.ratingLoading = false;
        this.showVideo = false;
        this.stars = 0;
        this.rated = false;
        this.video = this.navParams.get('video');
    }
    VideoDescriptionPage.prototype.ionViewDidEnter = function () {
        this.updateVideoDetails();
    };
    VideoDescriptionPage.prototype.updateVideoDetails = function () {
        var _this = this;
        var data = {
            'uid': this.userProvider.user.uid,
            'tid': this.video.tid
        };
        this.userProvider.taskDetails(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.video = success.data;
                //console.log(success.data);
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    VideoDescriptionPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad VideoDescriptionPage');
    };
    VideoDescriptionPage.prototype.userDetail = function () {
        this.navCtrl.push(UserProfilePage);
    };
    VideoDescriptionPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    VideoDescriptionPage.prototype.playVideo = function () {
        var _this = this;
        var data = {
            'uid': this.userProvider.user.uid,
            'tid': this.video.tid
        };
        this.loading = true;
        this.userProvider.playVideo(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                //console.log(success);
                _this.showVideo = true;
                _this.videoElement = _this.videop.nativeElement;
                _this.videoElement.play();
                _this.userProvider.userUpdatedDetails();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    VideoDescriptionPage.prototype.addRating = function () {
        var _this = this;
        var data = {
            'uid': this.userProvider.user.uid,
            'tid': this.video.tid,
            'rating': this.stars
        };
        this.rated = true;
        this.ratingLoading = true;
        this.userProvider.addRating(data).subscribe(function (success) {
            _this.ratingLoading = false;
            if (success.message == "Successful") {
                //console.log(success);
                _this.rated = true;
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.rated = false;
            _this.ratingLoading = false;
            //console.log(JSON.stringify(error))
        });
        //console.log('New star rating: ' + this.stars);
    };
    __decorate([
        ViewChild("videop"),
        __metadata("design:type", ElementRef)
    ], VideoDescriptionPage.prototype, "videop", void 0);
    VideoDescriptionPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-video-description',
            templateUrl: 'video-description.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            NavParams])
    ], VideoDescriptionPage);
    return VideoDescriptionPage;
}());
export { VideoDescriptionPage };
//# sourceMappingURL=video-description.js.map