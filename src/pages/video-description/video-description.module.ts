import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoDescriptionPage } from './video-description';

@NgModule({
  declarations: [
    VideoDescriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoDescriptionPage),
  ],
})
export class VideoDescriptionPageModule {}
