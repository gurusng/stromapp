import {Component, ElementRef, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
import { UserProfilePage } from '../user-profile/user-profile';
import {ScreenOrientation} from "@ionic-native/screen-orientation";

/**
 * Generated class for the VideoDescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-video-description',
    templateUrl: 'video-description.html',
})
export class VideoDescriptionPage {

    @ViewChild("videop") videop: ElementRef; // binds to #video in video.html
    videoElement: HTMLVideoElement;

    video:any = null;
    loading:boolean = false;
    ratingLoading:boolean = false;
    showVideo:boolean = false;

    stars:number = 0;

    rated:boolean = false;

    constructor(public navCtrl: NavController,
        public userProvider: UserProvider,
        public screenOrientation : ScreenOrientation,
        public navParams: NavParams) {


        this.video = this.navParams.get('video');

    }

    ionViewDidEnter(){
        this.updateVideoDetails();
    }

    updateVideoDetails(){
        let data = {
            'uid' : this.userProvider.user.uid,
            'tid' : this.video.tid
        };
        this.userProvider.taskDetails(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.video = success.data;
                //console.log(success.data);

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad VideoDescriptionPage');
    }

    userDetail() {
        this.navCtrl.push(UserProfilePage);
    }

    goBack() {
        this.navCtrl.pop();
    }

    playVideo(){

        let data = {
            'uid' : this.userProvider.user.uid,
            'tid' : this.video.tid
        };

        this.loading = true;
        this.userProvider.playVideo(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                //console.log(success);
                this.showVideo = true;
                this.videoElement = this.videop.nativeElement;
                this.videoElement.play();

                this.userProvider.userUpdatedDetails();

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    addRating(){

        let data = {
            'uid' : this.userProvider.user.uid,
            'tid' : this.video.tid,
            'rating' : this.stars
        };

        this.rated = true;
        this.ratingLoading = true;
        this.userProvider.addRating(data).subscribe((success:any)=>{
            this.ratingLoading = false;
            if(success.message == "Successful"){
                //console.log(success);

                this.rated = true;


            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.rated = false;
            this.ratingLoading = false;
            //console.log(JSON.stringify(error))
        });

        //console.log('New star rating: ' + this.stars);
    }

}
