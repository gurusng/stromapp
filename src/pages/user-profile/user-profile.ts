import { Component } from '@angular/core';
import {
    ActionSheetController,
    AlertController,
    App,
    IonicPage,
    NavController,
    NavParams,
} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import {UserProvider} from "../../providers/user/user";
import {IndexPage} from "../index";
import { CashoutPage } from '../cashout/cashout';
import {ImagePicker} from "@ionic-native/image-picker";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {FileTransferObject, FileTransfer, FileUploadOptions} from "@ionic-native/file-transfer";
import { AddCardPage } from '../add-card/add-card';
import {MyTasksPage} from "../my-tasks/my-tasks";
import {MyAdsPage} from "../my-ads/my-ads";
import {EditProfilePage} from "../edit-profile/edit-profile";


/**
 * Generated class for the UserProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {


    photoLoading:boolean = false;
    loading:boolean = false;

    constructor(public navCtrl: NavController,
                private app: App,
                private camera : Camera,
                public userProvider: UserProvider,
                private imagePicker: ImagePicker,
                public transfer: FileTransfer,
                public alertCtrl: AlertController,
                public actionSheetCtrl: ActionSheetController,
                public navParams: NavParams) {

    }

    ionViewDidLoad() {
        this.userProvider.userUpdatedDetails();

    }

    takePhoto() {

        const options: CameraOptions = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };

        this.camera.getPicture(options).then((imagePath) => {

            // let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            // let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            const fileTransfer: FileTransferObject = this.transfer.create();


            let option:FileUploadOptions = {
                fileKey: "image",
                fileName: "image.jpg",
                chunkedMode: false,
                mimeType: "multipart/form-data",
                params : {uid: this.userProvider.user.uid},
            };

            let self = this;

            self.photoLoading = true;

            fileTransfer.upload(imagePath , this.userProvider.url + 'change-profile-photo', option).then(data => {

                self.userProvider.userUpdatedDetails();
                self.userProvider.showAlert("Successful","Your profile image has been updated.");

                self.photoLoading = false;

            }, err => {
                //console.log(JSON.stringify(err));
                self.userProvider.showAlert("Error","Please try again");

                self.photoLoading = false;
            });

        }, (error) => {
            //console.log(JSON.stringify(error));
            this.userProvider.showAlert("Error","Please try again");

            this.photoLoading = false;

            // Handle error
        });
    }


    uploadPhoto(){

        let options = {
            maximumImagesCount: 1,
            width: 800,
            height: 800,
            quality: 100
        };

        this.imagePicker.getPictures(options).then((results) => {

            for (var i = 0; i < results.length; i++) {

                let option:FileUploadOptions = {
                    fileKey: "image",
                    fileName: "image.jpg",
                    chunkedMode: false,
                    mimeType: "multipart/form-data",
                    params : {uid: this.userProvider.user.uid},
                };

                const fileTransfer: FileTransferObject = this.transfer.create();


                let self = this;

                self.photoLoading = true;
                fileTransfer.upload(results[i] , this.userProvider.url + 'change-profile-photo', option).then(data => {

                    self.userProvider.showAlert("Successful","Your profile image has been updated.");
                    self.userProvider.userUpdatedDetails();
                    self.photoLoading = false;

                }, err => {
                    //console.log(err);
                    self.userProvider.showAlert("Error","Please try again");
                    self.photoLoading = false;
                });

            }

        }, (err) => {
            this.userProvider.showAlert("Error","Failed to select photo. Please try again.");
        });
    }

    photoAction() {

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Take Photo',
                    handler: () => {
                        this.takePhoto();
                    }
                },{

                    text: 'Upload Photo',
                    handler: () => {
                        this.uploadPhoto();
                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    }




    goBack() {
        this.navCtrl.push(TabsPage);
    }

    goToMyTasks(){
        this.app.getRootNav().push(MyTasksPage);
    }

    goToMyAds(){
        this.app.getRootNav().push(MyAdsPage);
    }

    logout(){
        this.userProvider.logout();
        this.app.getRootNav().setRoot(IndexPage);

    }

    cashout() {
    this.navCtrl.setRoot(CashoutPage);
    }

    editProfile(){
        this.navCtrl.push(EditProfilePage);
    }
    addCard() {
        this.navCtrl.push(AddCardPage)
    }

    deleteCard(card){
        let data = {
            'cardid' : card.cardid
        };
        this.loading = true;
        this.userProvider.deleteCard(data).subscribe((success:any)=>{

            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert("Success","Card Deleted.")
                this.userProvider.userUpdatedDetails();

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    confirmCardDelete(card) {
        const confirm = this.alertCtrl.create({
            title: 'Confirmation',
            message: 'Are you sure you want to delete this card?',
            buttons: [
                {
                    text: 'No',
                    handler: () => {
                        //console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.deleteCard(card);
                        //console.log('Agree clicked');
                    }
                }
            ]
        });
        confirm.present();
    }

}
