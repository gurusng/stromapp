var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ActionSheetController, AlertController, App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { UserProvider } from "../../providers/user/user";
import { IndexPage } from "../index";
import { CashoutPage } from '../cashout/cashout';
import { ImagePicker } from "@ionic-native/image-picker";
import { Camera } from "@ionic-native/camera";
import { FileTransfer } from "@ionic-native/file-transfer";
import { AddCardPage } from '../add-card/add-card';
import { MyTasksPage } from "../my-tasks/my-tasks";
import { MyAdsPage } from "../my-ads/my-ads";
import { EditProfilePage } from "../edit-profile/edit-profile";
/**
 * Generated class for the UserProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UserProfilePage = /** @class */ (function () {
    function UserProfilePage(navCtrl, app, camera, userProvider, imagePicker, transfer, alertCtrl, actionSheetCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.camera = camera;
        this.userProvider = userProvider;
        this.imagePicker = imagePicker;
        this.transfer = transfer;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.navParams = navParams;
        this.photoLoading = false;
        this.loading = false;
    }
    UserProfilePage.prototype.ionViewDidLoad = function () {
        this.userProvider.userUpdatedDetails();
    };
    UserProfilePage.prototype.takePhoto = function () {
        var _this = this;
        var options = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };
        this.camera.getPicture(options).then(function (imagePath) {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            var fileTransfer = _this.transfer.create();
            var option = {
                fileKey: "image",
                fileName: "image.jpg",
                chunkedMode: false,
                mimeType: "multipart/form-data",
                params: { uid: _this.userProvider.user.uid },
            };
            var self = _this;
            self.photoLoading = true;
            fileTransfer.upload(imagePath, _this.userProvider.url + 'change-profile-photo', option).then(function (data) {
                self.userProvider.userUpdatedDetails();
                self.userProvider.showAlert("Successful", "Your profile image has been updated.");
                self.photoLoading = false;
            }, function (err) {
                //console.log(JSON.stringify(err));
                self.userProvider.showAlert("Error", "Please try again");
                self.photoLoading = false;
            });
        }, function (error) {
            //console.log(JSON.stringify(error));
            _this.userProvider.showAlert("Error", "Please try again");
            _this.photoLoading = false;
            // Handle error
        });
    };
    UserProfilePage.prototype.uploadPhoto = function () {
        var _this = this;
        var options = {
            maximumImagesCount: 1,
            width: 800,
            height: 800,
            quality: 100
        };
        this.imagePicker.getPictures(options).then(function (results) {
            var _loop_1 = function () {
                var option = {
                    fileKey: "image",
                    fileName: "image.jpg",
                    chunkedMode: false,
                    mimeType: "multipart/form-data",
                    params: { uid: _this.userProvider.user.uid },
                };
                var fileTransfer = _this.transfer.create();
                var self_1 = _this;
                self_1.photoLoading = true;
                fileTransfer.upload(results[i], _this.userProvider.url + 'change-profile-photo', option).then(function (data) {
                    self_1.userProvider.showAlert("Successful", "Your profile image has been updated.");
                    self_1.userProvider.userUpdatedDetails();
                    self_1.photoLoading = false;
                }, function (err) {
                    //console.log(err);
                    self_1.userProvider.showAlert("Error", "Please try again");
                    self_1.photoLoading = false;
                });
            };
            for (var i = 0; i < results.length; i++) {
                _loop_1();
            }
        }, function (err) {
            _this.userProvider.showAlert("Error", "Failed to select photo. Please try again.");
        });
    };
    UserProfilePage.prototype.photoAction = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Take Photo',
                    handler: function () {
                        _this.takePhoto();
                    }
                }, {
                    text: 'Upload Photo',
                    handler: function () {
                        _this.uploadPhoto();
                    }
                }, {
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: function () {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    };
    UserProfilePage.prototype.goBack = function () {
        this.navCtrl.push(TabsPage);
    };
    UserProfilePage.prototype.goToMyTasks = function () {
        this.app.getRootNav().push(MyTasksPage);
    };
    UserProfilePage.prototype.goToMyAds = function () {
        this.app.getRootNav().push(MyAdsPage);
    };
    UserProfilePage.prototype.logout = function () {
        this.userProvider.logout();
        this.app.getRootNav().setRoot(IndexPage);
    };
    UserProfilePage.prototype.cashout = function () {
        this.navCtrl.setRoot(CashoutPage);
    };
    UserProfilePage.prototype.editProfile = function () {
        this.navCtrl.push(EditProfilePage);
    };
    UserProfilePage.prototype.addCard = function () {
        this.navCtrl.push(AddCardPage);
    };
    UserProfilePage.prototype.deleteCard = function (card) {
        var _this = this;
        var data = {
            'cardid': card.cardid
        };
        this.loading = true;
        this.userProvider.deleteCard(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Success", "Card Deleted.");
                _this.userProvider.userUpdatedDetails();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    UserProfilePage.prototype.confirmCardDelete = function (card) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Confirmation',
            message: 'Are you sure you want to delete this card?',
            buttons: [
                {
                    text: 'No',
                    handler: function () {
                        //console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.deleteCard(card);
                        //console.log('Agree clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    UserProfilePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-user-profile',
            templateUrl: 'user-profile.html',
        }),
        __metadata("design:paramtypes", [NavController,
            App,
            Camera,
            UserProvider,
            ImagePicker,
            FileTransfer,
            AlertController,
            ActionSheetController,
            NavParams])
    ], UserProfilePage);
    return UserProfilePage;
}());
export { UserProfilePage };
//# sourceMappingURL=user-profile.js.map