import {Component} from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";
import {UserProvider} from "../../providers/user/user";
import {UserProfilePage} from "../user-profile/user-profile";

/**
 * Generated class for the CashoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-cashout',
    templateUrl: 'cashout.html',
})
export class CashoutPage {

    bankName:string = '';
    section: string = 'transfer';
    accountNumber:string;
    accountName:string;
    transferAmount:number = 0;
    transferAmountInPoints:number;
    airtimeAmount:number = 0;
    airtimeAmountInPoints:number =0;
    password:string;
    network:string;

    phoneNumber:string;

    //loadings
    loadingAccount:boolean = false;
    loading:boolean = false;



    constructor(public navCtrl: NavController,
                private app: App,
                public userProvider: UserProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad CashoutPage');
    }

    goBack() {
        this.app.getRootNav().setRoot(TabsPage);
        // this.navCtrl.setRoot(TabsPage);
    }

    updateSection(section) {
        this.section = section;
    }

    userDetail() {
        this.app.getRootNav().setRoot(UserProfilePage);
    }

    cashoutTransfer(){
        if(this.transferAmount < 5000 ) {
            this.userProvider.showAlert("Error","You cannot withdraw less than 5000 naira");
            return;
        }
        if(this.transferAmount == null || this.transferAmount <= 0) {
            this.userProvider.showAlert("Error", "Please fill all fields");
            return;
        }
        if(this.password == null || this.password == "") {
            this.userProvider.showAlert("Error", "Please fill all fields");
            return;
        }

        let data = {
            'uid' : this.userProvider.user.uid,
            'amount' : this.transferAmount,
            'accountBank' : this.bankName,
            'accountNumber' : this.accountNumber,
            'accountName' : this.accountName,
        };


        let user = {
            'email' : this.userProvider.user.email,
            'password' : this.password
        };

        this.loading = true;

        this.userProvider.signIn(user).subscribe((success:any)=>{
            if(success.message == "Successful"){

                this.userProvider.cashoutTransfer(data).subscribe((success:any)=>{
                    this.loading = false;
                    if(success.message == "Successful"){
                        this.userProvider.showAlert("Success", "Transfer Completed.");

                        this.userProvider.userUpdatedDetails();
                        this.accountName = null;
                        this.accountNumber = null;
                        this.bankName = null;
                        this.password = null;
                        this.transferAmountInPoints = null;
                        this.transferAmount = null;

                    } else{
                        this.userProvider.showAlert("Error",success.message);
                    }

                },(error)=>{
                    this.loading = false;
                    //console.log(JSON.stringify(error))
                });


            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });




    }

    cashoutAirtime(){
        if(this.airtimeAmount < 1000 ) this.userProvider.showAlert("Error", "You cannot withdraw less than 1000 naira");

        let data = {
            'uid' : this.userProvider.user.uid,
            'phone' : this.phoneNumber,
            'amount' : this.airtimeAmount,
            'mobileNetwork' : this.network
        };

        this.loading = true;

        let user = {
            'email' : this.userProvider.user.email,
            'password' : this.password
        };

        this.userProvider.signIn(user).subscribe((success:any)=>{

            this.loading = false;
            if(success.message == "Successful"){

                this.userProvider.cashoutAirtime(data).subscribe((success:any)=>{
                    this.loading = false;
                    if(success.message == "Successful"){
                        this.userProvider.showAlert("Success", "Airtime cashout request has been sent for approval.");
                        this.userProvider.userUpdatedDetails();

                        this.phoneNumber = "";
                        this.airtimeAmount = null;


                    } else{
                        this.userProvider.showAlert("Error",success.data.Message);
                    }

                },(error)=>{
                    this.loading = false;
                    //console.log(JSON.stringify(error))
                }); // cashout airtime


            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });



    }

    resolveAccountName(){

        if(this.accountNumber == null){
            return;
        }
        let data = {
            'accountNumber' : this.accountNumber,
            'accountBank'   : this.bankName
        };

        this.loadingAccount = true;
        this.accountName = null;
        this.userProvider.resolveAccountName(data).subscribe((success:any)=>{
            this.loadingAccount = false;
            if(success.message == "ACCOUNT RESOLVED"){
                this.accountName = success.data.data.accountname;

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loadingAccount = false;
            //console.log(JSON.stringify(error))
        });

    }

    checkBank(){
        if(this.bankName == null || this.bankName == "")
            this.userProvider.showAlert("Error","Please select a bank first");
    }

    calculateAmount(){

        //console.log(this.airtimeAmount);
        //console.log(this.airtimeAmountInPoints);
        this.airtimeAmountInPoints = Math.round( this.airtimeAmount / 0.825) ;

        if(this.airtimeAmountInPoints > this.userProvider.user.points) {
            this.userProvider.showAlert("Error", "You do not have enough points.");
            return;
        }

    }

    calculateTransferAmount(){

        if(this.transferAmountInPoints > this.userProvider.user.points) this.transferAmountInPoints = this.userProvider.user.points;

        this.transferAmount = Math.round(((100 - 17.5) / 100 ) * this.transferAmountInPoints);
    }

}
