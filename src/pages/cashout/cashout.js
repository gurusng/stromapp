var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { UserProvider } from "../../providers/user/user";
import { UserProfilePage } from "../user-profile/user-profile";
/**
 * Generated class for the CashoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CashoutPage = /** @class */ (function () {
    function CashoutPage(navCtrl, app, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.bankName = '';
        this.section = 'transfer';
        this.transferAmount = 0;
        this.airtimeAmount = 0;
        this.airtimeAmountInPoints = 0;
        //loadings
        this.loadingAccount = false;
        this.loading = false;
    }
    CashoutPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad CashoutPage');
    };
    CashoutPage.prototype.goBack = function () {
        this.app.getRootNav().setRoot(TabsPage);
        // this.navCtrl.setRoot(TabsPage);
    };
    CashoutPage.prototype.updateSection = function (section) {
        this.section = section;
    };
    CashoutPage.prototype.userDetail = function () {
        this.app.getRootNav().setRoot(UserProfilePage);
    };
    CashoutPage.prototype.cashoutTransfer = function () {
        var _this = this;
        if (this.transferAmount < 5000) {
            this.userProvider.showAlert("Error", "You cannot withdraw less than 5000 naira");
            return;
        }
        if (this.transferAmount == null || this.transferAmount <= 0) {
            this.userProvider.showAlert("Error", "Please fill all fields");
            return;
        }
        if (this.password == null || this.password == "") {
            this.userProvider.showAlert("Error", "Please fill all fields");
            return;
        }
        var data = {
            'uid': this.userProvider.user.uid,
            'amount': this.transferAmount,
            'accountBank': this.bankName,
            'accountNumber': this.accountNumber,
            'accountName': this.accountName,
        };
        var user = {
            'email': this.userProvider.user.email,
            'password': this.password
        };
        this.loading = true;
        this.userProvider.signIn(user).subscribe(function (success) {
            if (success.message == "Successful") {
                _this.userProvider.cashoutTransfer(data).subscribe(function (success) {
                    _this.loading = false;
                    if (success.message == "Successful") {
                        _this.userProvider.showAlert("Success", "Transfer Completed.");
                        _this.userProvider.userUpdatedDetails();
                        _this.accountName = null;
                        _this.accountNumber = null;
                        _this.bankName = null;
                        _this.password = null;
                        _this.transferAmountInPoints = null;
                        _this.transferAmount = null;
                    }
                    else {
                        _this.userProvider.showAlert("Error", success.message);
                    }
                }, function (error) {
                    _this.loading = false;
                    //console.log(JSON.stringify(error))
                });
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    CashoutPage.prototype.cashoutAirtime = function () {
        var _this = this;
        if (this.airtimeAmount < 1000)
            this.userProvider.showAlert("Error", "You cannot withdraw less than 1000 naira");
        var data = {
            'uid': this.userProvider.user.uid,
            'phone': this.phoneNumber,
            'amount': this.airtimeAmount
        };
        this.loading = true;
        var user = {
            'email': this.userProvider.user.email,
            'password': this.password
        };
        this.userProvider.signIn(user).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.cashoutAirtime(data).subscribe(function (success) {
                    _this.loading = false;
                    if (success.data.Status == "success") {
                        _this.userProvider.showAlert("Success", "Airtime has been sent");
                        _this.userProvider.userUpdatedDetails();
                        _this.phoneNumber = "";
                        _this.airtimeAmount = null;
                    }
                    else {
                        _this.userProvider.showAlert("Error", success.data.Message);
                    }
                }, function (error) {
                    _this.loading = false;
                    //console.log(JSON.stringify(error))
                }); // cashout airtime
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    CashoutPage.prototype.resolveAccountName = function () {
        var _this = this;
        if (this.accountNumber == null) {
            return;
        }
        var data = {
            'accountNumber': this.accountNumber,
            'accountBank': this.bankName
        };
        this.loadingAccount = true;
        this.accountName = null;
        this.userProvider.resolveAccountName(data).subscribe(function (success) {
            _this.loadingAccount = false;
            if (success.message == "ACCOUNT RESOLVED") {
                _this.accountName = success.data.data.accountname;
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loadingAccount = false;
            //console.log(JSON.stringify(error))
        });
    };
    CashoutPage.prototype.checkBank = function () {
        if (this.bankName == null || this.bankName == "")
            this.userProvider.showAlert("Error", "Please select a bank first");
    };
    CashoutPage.prototype.calculateAmount = function () {
        //console.log(this.airtimeAmount);
        //console.log(this.airtimeAmountInPoints);
        this.airtimeAmountInPoints = Math.round(this.airtimeAmount / 0.825);
        if (this.airtimeAmountInPoints > this.userProvider.user.points) {
            this.userProvider.showAlert("Error", "You do not have enough points.");
            return;
        }
    };
    CashoutPage.prototype.calculateTransferAmount = function () {
        if (this.transferAmountInPoints > this.userProvider.user.points)
            this.transferAmountInPoints = this.userProvider.user.points;
        this.transferAmount = Math.round(((100 - 17.5) / 100) * this.transferAmountInPoints);
    };
    CashoutPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cashout',
            templateUrl: 'cashout.html',
        }),
        __metadata("design:paramtypes", [NavController,
            App,
            UserProvider,
            NavParams])
    ], CashoutPage);
    return CashoutPage;
}());
export { CashoutPage };
//# sourceMappingURL=cashout.js.map