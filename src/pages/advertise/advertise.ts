import {Component, ElementRef, ViewChild} from '@angular/core';
import {
    ActionSheetController,
    Events,
    IonicPage,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {UserProfilePage} from '../user-profile/user-profile';
import {UserProvider} from "../../providers/user/user";
import {CameraOptions, Camera} from "@ionic-native/camera";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {ImagePicker} from "@ionic-native/image-picker";
import {HttpClient} from "@angular/common/http";


/**
 * Generated class for the AdvertisePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var window;

@IonicPage()
@Component({
    selector: 'page-advertise',
    templateUrl: 'advertise.html',
})

export class AdvertisePage {
    public expiry: string = null;
    public maxUsers: number;
    public points: number;
    public verificationDuration: number;
    public longDescription: string;
    public name: string;
    public minDate:any;
    public maxDate:any;
    location:string;
    videoType: string = 'videop2v';
    photoLoading: boolean = false;
    loading: boolean = false;
    section: string = 'social';
    files: any[] = [];
    images: any[] = [];


    @ViewChild("uploadVideo") uploadVideo: ElementRef;
    uploadVideoElement: HTMLVideoElement;

    @ViewChild("uploadImages") uploadImages: ElementRef;
    uploadImagesElement: HTMLVideoElement;


    constructor(public navCtrl: NavController,
                private imagePicker: ImagePicker,
                private camera: Camera,
                public platform: Platform,
                public http: HttpClient,
                public transfer: FileTransfer,
                public events : Events,
                public actionSheetCtrl: ActionSheetController,
                public userProvider: UserProvider,
                public navParams: NavParams) {

        // this.minDate = new Date().getFullYear() + '-' + new Date().getMonth() + 1 + '-' + new Date().getDate();
        // this.maxDate  = new Date().getFullYear() + 5;
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad AdvertisePage');
    }

    ionViewDidEnter(){
        this.images = [];
    }

    takeVideo() {

        const options: CameraOptions = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.VIDEO,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };

        this.camera.getPicture(options).then((imagePath) => {

            // let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            // let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            const fileTransfer: FileTransferObject = this.transfer.create();


            let option: FileUploadOptions = {
                fileKey: "image",
                fileName: "image.jpg",
                // chunkedMode: false,
                // mimeType: "multipart/form-data",
                params: {uid: this.userProvider.user.uid},
            };

            let self = this;

            self.photoLoading = true;

            fileTransfer.upload(imagePath, this.userProvider.url + 'change-profile-photo', option).then(data => {

                self.userProvider.userUpdatedDetails();
                self.userProvider.showAlert("Successful", "Your profile image has been updated.");

                self.photoLoading = false;

            }, err => {
                //console.log(JSON.stringify(err));
                self.userProvider.showAlert("Error", "Please try again");

                self.photoLoading = false;
            });

        }, (error) => {
            //console.log(JSON.stringify(error));
            this.userProvider.showAlert("Error", "Please try again");

            this.photoLoading = false;

            // Handle error
        });
    }


    takePhoto() {

        const options: CameraOptions = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };

        this.camera.getPicture(options).then((imagePath) => {

            let name = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            // let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

            this.images.push(imagePath);

        }, (error) => {
            //console.log(error)
            // Handle error
        });
    }


    createFile(url){

        let name = url.substr(url.lastIndexOf('/') + 1);


        window.resolveLocalFileSystemURL(url, (fileEntry) => {

            //console.log(fileEntry);
            fileEntry.file((resFile) => {

                //console.log(resFile);
                var reader = new FileReader();
                reader.onloadend = (evt: any) => {

                    var imgBlob: any = new Blob([evt.target.result], {type: 'image/jpeg'});

                    return new File(imgBlob,name);

                };



            });

        });

    }

    uploadPhoto(){

        let options = {
            maximumImagesCount: 10,
            width: 800,
            height: 800,
            quality: 100
        };

        this.imagePicker.getPictures(options).then((results) => {

            for (var i = 0; i < results.length; i++) {

                this.images.push(results[i]);

            }

        }, (err) => {
            this.userProvider.showAlert("Error","Failed to select photo. Please try again.");
        });
    }

    androidPhotoAction() {

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Add From Camera',
                    handler: () => {
                        this.takePhoto();
                    }
                },{

                    text: 'Add From Gallery',
                    handler: () => {
                        this.uploadPhoto();
                    }
                },{
                    text: 'Clear Images',
                    handler: () => {
                        this.images = [];
                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    }



    photoAction() {

        if(this.platform.is('android')){
            this.androidPhotoAction();
            return;
        }

        this.images = [];
        this.uploadImages.nativeElement.click();
    }

    videoAction() {

        this.files = [];
        this.uploadVideo.nativeElement.click();
    }


    userDetail() {
        this.navCtrl.push(UserProfilePage);
    }

    updateSection(section) {
        this.section = section;
    }

    startVideoTask() {

        var formData: any = new FormData();

        //console.log(formData.get('uid'));
        formData.append('uid', String(this.userProvider.user.uid));
        formData.append('expiry', this.expiry);
        formData.append('maxUsers', String(this.maxUsers));
        formData.append('points', String(this.points));
        formData.append('verificationDuration', 1);
        formData.append('longDescription', String(this.longDescription));
        formData.append('name', String(this.name));
        formData.append('type', String(this.videoType));
        for (let i = 0; i < this.files.length; i++) {
            formData.append('video', this.files[i], this.files[i].name);
        }

        //console.log(String(this.userProvider.user.uid));

        this.loading = true;
        this.userProvider.addVideoTask(formData).subscribe((success: any) => {

            this.loading = false;
            this.loading = false;
            if (success.message == "Successful") {

                this.userProvider.showAlert("Successful", "Your task has sent for approval.");
                this.expiry = null;
                this.maxUsers = null;
                this.points = null;
                this.verificationDuration = null;
                this.longDescription = null;
                this.name = null;

            } else {
                this.userProvider.showAlert("Error", success.message);
            }

            //console.log(JSON.stringify(success));
        }, (error) => {
            this.loading = false;
            //console.log(JSON.stringify(error))
        });


    }

    startCommunityTask(){
        if(this.platform.is('android')){
            this.startCommunityTaskAndroid();
            return;
        }

        this.startCommunityTaskIOS();

    }

    startCommunityTaskAndroid(){

        let options: FileUploadOptions = {
            fileKey: 'images[]',
            fileName: 'upload.jpg',
            params : {
                'uid' : this.userProvider.user.uid,
                'expiry' : this.expiry,
                'maxUsers' : this.maxUsers,
                'points' : this.points,
                'location' : this.location,
                'verificationDuration' : 1,
                'longDescription' : this.longDescription,
                'name' : this.name
            }
        };

        this.loading = true;
        let self = this;

        const fileTransfer: FileTransferObject = this.transfer.create();

        fileTransfer.upload(this.images[0], this.userProvider.url + 'add-community-task', options)
            .then((success:any) => {

                self.loading = false;
                let data = JSON.parse(success.response);

                if(data.message == "Successful"){

                    this.userProvider.showAlert("Successful", "Your task has been added. We are adding the images to it.");
                    this.expiry = null;
                    this.maxUsers = null;
                    this.points = null;
                    this.verificationDuration = null;
                    this.longDescription = null;

                    this.name = null;


                    if(this.images.length > 1){ // send remaining pics
                        let sentCount =  1;
                        for(let i = 1; i < this.images.length; i++){

                            let options: FileUploadOptions = {
                                fileKey: 'image',
                                fileName: 'upload.jpg',
                                params : {
                                    'uid' : self.userProvider.user.uid,
                                    'tid' : data.data.tid,
                                }
                            };

                            fileTransfer.upload(this.images[i], this.userProvider.url + 'add-community-task-image', options)
                                .then((success:any) => {
                                    if(sentCount == this.images.length){
                                        this.images = [];

                                    }
                                    //console.log(success);
                                },(error)=>{

                                    this.userProvider.showToast("Some Images didn't upload successfully. Please try again with a faster network");
                                    //console.log(error)
                                });

                        }

                    }


                } else {
                    self.userProvider.showAlert("Error",data.message);

                }

            }, (error) => {

                // error
                //console.log(JSON.stringify(error));
                self.loading = false;

            })


    }

    // startCommunityTaskAndroid(){
    //     let data = {
    //         'uid' : this.userProvider.user.uid,
    //         'expiry' : this.expiry,
    //         'maxUsers' : this.maxUsers,
    //         'points' : this.points,
    //         'location' : this.location,
    //         'verificationDuration' : 1,
    //         'longDescription' : this.longDescription,
    //         'name' : this.name
    //     };
    //
    //
    //     this.loading = true;
    //     this.userProvider.startCommunityTaskAndroid(data,this.images);
    //
    //     this.events.subscribe("community-finish",()=>{
    //         this.userProvider.showAlert("Successful", "Your task has been added.");
    //         this.expiry = null;
    //         this.maxUsers = null;
    //         this.points = null;
    //         this.verificationDuration = null;
    //         this.longDescription = null;
    //
    //         this.loading = false;
    //         this.name = null;
    //         this.images = [];
    //
    //     });
    //
    //     this.events.subscribe("community-finish-some",()=>{
    //         this.userProvider.showAlert("Successful", "Your task has been added, not all images uploaded. You can try again with a faster network");
    //         this.expiry = null;
    //         this.maxUsers = null;
    //         this.points = null;
    //         this.verificationDuration = null;
    //         this.longDescription = null;
    //
    //         this.name = null;
    //         this.images = [];
    //         this.loading = false;
    //
    //     });
    //
    //     this.events.subscribe("community-failed",()=>{
    //
    //     });
    // }



    startCommunityTaskIOS() {

        //console.log(this.images);

        var formData: any = new FormData();

        formData.append('uid', String(this.userProvider.user.uid));
        formData.append('expiry', this.expiry);
        formData.append('maxUsers', String(this.maxUsers));
        formData.append('points', String(this.points));
        formData.append('location', String(this.location));
        formData.append('verificationDuration', 1);
        formData.append('longDescription', String(this.longDescription));
        formData.append('name', String(this.name));
        for (let i = 0; i < this.images.length; i++) {
            formData.append('images[]', this.images[i], this.images[i].name);
        }

        this.loading = true;
        this.userProvider.addCommunityTask(formData).subscribe((success: any) => {

            this.loading = false;

            if (success.message == "Successful") {

                this.userProvider.showAlert("Successful", "Your task has been added.");
                this.expiry = null;
                this.maxUsers = null;
                this.points = null;
                this.verificationDuration = null;
                this.longDescription = null;

                this.name = null;
                this.images = [];

            } else {
                this.userProvider.showAlert("Error", success.message);
            }


            //console.log(JSON.stringify(success));
        }, (error) => {
            this.loading = false;
            this.userProvider.showAlert("Error","Something went wrong. Our engineers have been notified. Please try again.");
            //console.log(JSON.stringify(error))
        });


    }

    startSocialTask() {
        let data = {
            'uid': this.userProvider.user.uid,
            'expiry': this.expiry,
            'maxUsers': this.maxUsers,
            'points': this.points,
            'verificationDuration': this.verificationDuration,
            'longDescription': this.longDescription,
            'name': this.name
        };


        this.loading = true;

        this.userProvider.addSocialTask(data).subscribe((success: any) => {
            this.loading = false;
            if (success.message == "Successful") {
                this.userProvider.showAlert("Successful", "Your task has been added.");
                this.expiry = null;
                this.maxUsers = null;
                this.points = null;
                this.verificationDuration = null;
                this.longDescription = null;
                this.name = null;


            } else {
                this.userProvider.showAlert("Error", success.message);
            }

        }, (error) => {
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    setVideo(video) {
        this.videoType = video;
    }


    addVideo(files: any[]): void {

        this.validateVideo(files);

    }

    addImages(files: any[]): void {
        this.images = files;
    }

    validateVideo(files: any[]) {

        let self = this;

        for (let i = 0; i < files.length; i++) {
            var video = document.createElement('video');
            video.preload = 'metadata';

            video.onloadedmetadata = function () {

                window.URL.revokeObjectURL(video.src);

                if (video.duration < 10) {

                    self.userProvider.showAlert("Error", "Invalid Video! Video must be more than 10 seconds.");
                    return;
                }

                if (video.duration > 1200) {

                    self.userProvider.showAlert("Error", "Video too long. Each video can be a max of 20 minutes.");
                    return;
                }

                self.files = files; // add the video if valid

            };

            video.src = URL.createObjectURL(files[i]);

        }
    }

}
