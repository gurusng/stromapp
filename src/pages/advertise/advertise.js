var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ElementRef, ViewChild } from '@angular/core';
import { ActionSheetController, Events, IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { UserProfilePage } from '../user-profile/user-profile';
import { UserProvider } from "../../providers/user/user";
import { Camera } from "@ionic-native/camera";
import { FileTransfer } from "@ionic-native/file-transfer";
import { ImagePicker } from "@ionic-native/image-picker";
import { HttpClient } from "@angular/common/http";
var AdvertisePage = /** @class */ (function () {
    function AdvertisePage(navCtrl, imagePicker, camera, platform, http, transfer, events, actionSheetCtrl, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.imagePicker = imagePicker;
        this.camera = camera;
        this.platform = platform;
        this.http = http;
        this.transfer = transfer;
        this.events = events;
        this.actionSheetCtrl = actionSheetCtrl;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.expiry = null;
        this.videoType = 'videop2v';
        this.photoLoading = false;
        this.loading = false;
        this.section = 'social';
        this.files = [];
        this.images = [];
        // this.minDate = new Date().getFullYear() + '-' + new Date().getMonth() + 1 + '-' + new Date().getDate();
        // this.maxDate  = new Date().getFullYear() + 5;
    }
    AdvertisePage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad AdvertisePage');
    };
    AdvertisePage.prototype.ionViewDidEnter = function () {
        this.images = [];
    };
    AdvertisePage.prototype.takeVideo = function () {
        var _this = this;
        var options = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.VIDEO,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };
        this.camera.getPicture(options).then(function (imagePath) {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            var fileTransfer = _this.transfer.create();
            var option = {
                fileKey: "image",
                fileName: "image.jpg",
                // chunkedMode: false,
                // mimeType: "multipart/form-data",
                params: { uid: _this.userProvider.user.uid },
            };
            var self = _this;
            self.photoLoading = true;
            fileTransfer.upload(imagePath, _this.userProvider.url + 'change-profile-photo', option).then(function (data) {
                self.userProvider.userUpdatedDetails();
                self.userProvider.showAlert("Successful", "Your profile image has been updated.");
                self.photoLoading = false;
            }, function (err) {
                //console.log(JSON.stringify(err));
                self.userProvider.showAlert("Error", "Please try again");
                self.photoLoading = false;
            });
        }, function (error) {
            //console.log(JSON.stringify(error));
            _this.userProvider.showAlert("Error", "Please try again");
            _this.photoLoading = false;
            // Handle error
        });
    };
    AdvertisePage.prototype.takePhoto = function () {
        var _this = this;
        var options = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };
        this.camera.getPicture(options).then(function (imagePath) {
            var name = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            // let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            _this.images.push(imagePath);
        }, function (error) {
            //console.log(error)
            // Handle error
        });
    };
    AdvertisePage.prototype.createFile = function (url) {
        var name = url.substr(url.lastIndexOf('/') + 1);
        window.resolveLocalFileSystemURL(url, function (fileEntry) {
            //console.log(fileEntry);
            fileEntry.file(function (resFile) {
                //console.log(resFile);
                var reader = new FileReader();
                reader.onloadend = function (evt) {
                    var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                    return new File(imgBlob, name);
                };
            });
        });
    };
    AdvertisePage.prototype.uploadPhoto = function () {
        var _this = this;
        var options = {
            maximumImagesCount: 10,
            width: 800,
            height: 800,
            quality: 100
        };
        this.imagePicker.getPictures(options).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                _this.images.push(results[i]);
            }
        }, function (err) {
            _this.userProvider.showAlert("Error", "Failed to select photo. Please try again.");
        });
    };
    AdvertisePage.prototype.androidPhotoAction = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Add From Camera',
                    handler: function () {
                        _this.takePhoto();
                    }
                }, {
                    text: 'Add From Gallery',
                    handler: function () {
                        _this.uploadPhoto();
                    }
                }, {
                    text: 'Clear Images',
                    handler: function () {
                        _this.images = [];
                    }
                }, {
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: function () {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    };
    AdvertisePage.prototype.photoAction = function () {
        if (this.platform.is('android')) {
            this.androidPhotoAction();
            return;
        }
        this.images = [];
        this.uploadImages.nativeElement.click();
    };
    AdvertisePage.prototype.videoAction = function () {
        this.files = [];
        this.uploadVideo.nativeElement.click();
    };
    AdvertisePage.prototype.userDetail = function () {
        this.navCtrl.push(UserProfilePage);
    };
    AdvertisePage.prototype.updateSection = function (section) {
        this.section = section;
    };
    AdvertisePage.prototype.startVideoTask = function () {
        var _this = this;
        var formData = new FormData();
        //console.log(formData.get('uid'));
        formData.append('uid', String(this.userProvider.user.uid));
        formData.append('expiry', this.expiry);
        formData.append('maxUsers', String(this.maxUsers));
        formData.append('points', String(this.points));
        formData.append('verificationDuration', 1);
        formData.append('longDescription', String(this.longDescription));
        formData.append('name', String(this.name));
        formData.append('type', String(this.videoType));
        for (var i = 0; i < this.files.length; i++) {
            formData.append('video', this.files[i], this.files[i].name);
        }
        //console.log(String(this.userProvider.user.uid));
        for (var _i = 0, _a = formData.entries(); _i < _a.length; _i++) {
            var key = _a[_i];
            //console.log("in the for loop");
            //console.log(key[0] + ', ' + key[1])
        }
        //console.log(formData);
        this.loading = true;
        this.userProvider.addVideoTask(formData).subscribe(function (success) {
            _this.loading = false;
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Successful", "Your task has been added.");
                _this.expiry = null;
                _this.maxUsers = null;
                _this.points = null;
                _this.verificationDuration = null;
                _this.longDescription = null;
                _this.name = null;
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
            //console.log(JSON.stringify(success));
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AdvertisePage.prototype.startCommunityTask = function () {
        if (this.platform.is('android')) {
            this.startCommunityTaskAndroid();
            return;
        }
        this.startCommunityTaskIOS();
    };
    AdvertisePage.prototype.startCommunityTaskAndroid = function () {
        var _this = this;
        var options = {
            fileKey: 'images[]',
            fileName: 'upload.jpg',
            params: {
                'uid': this.userProvider.user.uid,
                'expiry': this.expiry,
                'maxUsers': this.maxUsers,
                'points': this.points,
                'location': this.location,
                'verificationDuration': 1,
                'longDescription': this.longDescription,
                'name': this.name
            }
        };
        this.loading = true;
        var self = this;
        var fileTransfer = this.transfer.create();
        fileTransfer.upload(this.images[0], this.userProvider.url + 'add-community-task', options)
            .then(function (success) {
            self.loading = false;
            var data = JSON.parse(success.response);
            //console.log(JSON.stringify(data.message));
            //console.log('data next');
            //console.log(JSON.stringify(data.data));
            if (data.message == "Successful") {
                _this.userProvider.showAlert("Successful", "Your task has been added. We are adding the images to it.");
                _this.expiry = null;
                _this.maxUsers = null;
                _this.points = null;
                _this.verificationDuration = null;
                _this.longDescription = null;
                _this.name = null;
                if (_this.images.length > 1) {
                    var sentCount_1 = 1;
                    for (var i = 1; i < _this.images.length; i++) {
                        var options_1 = {
                            fileKey: 'image',
                            fileName: 'upload.jpg',
                            params: {
                                'uid': self.userProvider.user.uid,
                                'tid': data.data.tid,
                            }
                        };
                        fileTransfer.upload(_this.images[i], _this.userProvider.url + 'add-community-task-image', options_1)
                            .then(function (success) {
                            if (sentCount_1 == _this.images.length) {
                                _this.images = [];
                            }
                            //console.log(success);
                        }, function (error) {
                            _this.userProvider.showToast("Some Images didn't upload successfully. Please try again with a faster network");
                            //console.log(error)
                        });
                    }
                }
            }
            else {
                self.userProvider.showAlert("Error", data.message);
            }
        }, function (error) {
            // error
            //console.log(JSON.stringify(error));
            self.loading = false;
        });
    };
    // startCommunityTaskAndroid(){
    //     let data = {
    //         'uid' : this.userProvider.user.uid,
    //         'expiry' : this.expiry,
    //         'maxUsers' : this.maxUsers,
    //         'points' : this.points,
    //         'location' : this.location,
    //         'verificationDuration' : 1,
    //         'longDescription' : this.longDescription,
    //         'name' : this.name
    //     };
    //
    //
    //     this.loading = true;
    //     this.userProvider.startCommunityTaskAndroid(data,this.images);
    //
    //     this.events.subscribe("community-finish",()=>{
    //         this.userProvider.showAlert("Successful", "Your task has been added.");
    //         this.expiry = null;
    //         this.maxUsers = null;
    //         this.points = null;
    //         this.verificationDuration = null;
    //         this.longDescription = null;
    //
    //         this.loading = false;
    //         this.name = null;
    //         this.images = [];
    //
    //     });
    //
    //     this.events.subscribe("community-finish-some",()=>{
    //         this.userProvider.showAlert("Successful", "Your task has been added, not all images uploaded. You can try again with a faster network");
    //         this.expiry = null;
    //         this.maxUsers = null;
    //         this.points = null;
    //         this.verificationDuration = null;
    //         this.longDescription = null;
    //
    //         this.name = null;
    //         this.images = [];
    //         this.loading = false;
    //
    //     });
    //
    //     this.events.subscribe("community-failed",()=>{
    //
    //     });
    // }
    AdvertisePage.prototype.startCommunityTaskIOS = function () {
        //console.log(this.images);
        var _this = this;
        var formData = new FormData();
        formData.append('uid', String(this.userProvider.user.uid));
        formData.append('expiry', this.expiry);
        formData.append('maxUsers', String(this.maxUsers));
        formData.append('points', String(this.points));
        formData.append('location', String(this.location));
        formData.append('verificationDuration', 1);
        formData.append('longDescription', String(this.longDescription));
        formData.append('name', String(this.name));
        for (var i = 0; i < this.images.length; i++) {
            formData.append('images[]', this.images[i], this.images[i].name);
        }
        for (var _i = 0, _a = formData.entries(); _i < _a.length; _i++) {
            var key = _a[_i];
            //console.log("in the for loop");
            //console.log(key[0] + ', ' + key[1])
        }
        //console.log(formData);
        this.loading = true;
        this.userProvider.addCommunityTask(formData).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Successful", "Your task has been added.");
                _this.expiry = null;
                _this.maxUsers = null;
                _this.points = null;
                _this.verificationDuration = null;
                _this.longDescription = null;
                _this.name = null;
                _this.images = [];
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
            //console.log(JSON.stringify(success));
        }, function (error) {
            _this.loading = false;
            _this.userProvider.showAlert("Error", "Something went wrong. Our engineers have been notified. Please try again.");
            //console.log(JSON.stringify(error))
        });
    };
    AdvertisePage.prototype.startSocialTask = function () {
        var _this = this;
        var data = {
            'uid': this.userProvider.user.uid,
            'expiry': this.expiry,
            'maxUsers': this.maxUsers,
            'points': this.points,
            'verificationDuration': this.verificationDuration,
            'longDescription': this.longDescription,
            'name': this.name
        };
        //console.log(JSON.stringify(data));
        this.loading = true;
        this.userProvider.addSocialTask(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Successful", "Your task has been added.");
                _this.expiry = null;
                _this.maxUsers = null;
                _this.points = null;
                _this.verificationDuration = null;
                _this.longDescription = null;
                _this.name = null;
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AdvertisePage.prototype.setVideo = function (video) {
        this.videoType = video;
    };
    AdvertisePage.prototype.addVideo = function (files) {
        this.validateVideo(files);
    };
    AdvertisePage.prototype.addImages = function (files) {
        this.images = files;
    };
    AdvertisePage.prototype.validateVideo = function (files) {
        var self = this;
        for (var i = 0; i < files.length; i++) {
            var video = document.createElement('video');
            video.preload = 'metadata';
            video.onloadedmetadata = function () {
                window.URL.revokeObjectURL(video.src);
                if (video.duration < 10) {
                    self.userProvider.showAlert("Error", "Invalid Video! Video must be more than 10 seconds.");
                    return;
                }
                self.files = files; // add the video if valid
            };
            video.src = URL.createObjectURL(files[i]);
        }
    };
    __decorate([
        ViewChild("uploadVideo"),
        __metadata("design:type", ElementRef)
    ], AdvertisePage.prototype, "uploadVideo", void 0);
    __decorate([
        ViewChild("uploadImages"),
        __metadata("design:type", ElementRef)
    ], AdvertisePage.prototype, "uploadImages", void 0);
    AdvertisePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-advertise',
            templateUrl: 'advertise.html',
        }),
        __metadata("design:paramtypes", [NavController,
            ImagePicker,
            Camera,
            Platform,
            HttpClient,
            FileTransfer,
            Events,
            ActionSheetController,
            UserProvider,
            NavParams])
    ], AdvertisePage);
    return AdvertisePage;
}());
export { AdvertisePage };
//# sourceMappingURL=advertise.js.map