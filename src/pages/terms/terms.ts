import {Component} from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserProfilePage} from "../user-profile/user-profile";
import {UserProvider} from "../../providers/user/user";

/**
 * Generated class for the TermsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-terms',
    templateUrl: 'terms.html',
})
export class TermsPage {

    constructor(public navCtrl: NavController,
                public userProvider: UserProvider,
                private app: App,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad TermsPage');
    }

    userDetail() {
        this.app.getRootNav().setRoot(UserProfilePage);
    }


}
