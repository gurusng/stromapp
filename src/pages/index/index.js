var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, MenuController } from 'ionic-angular';
import { SignInPage } from "../sign-in/sign-in";
import { SignUpPage } from "../sign-up/sign-up";
import { Storage } from "@ionic/storage";
import { UserProvider } from "../../providers/user/user";
import { TabsPage } from "../tabs/tabs";
/**
 * Generated class for the IndexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var IndexPage = /** @class */ (function () {
    function IndexPage(navCtrl, userProvider, storage, menuCtrl, events, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.storage = storage;
        this.menuCtrl = menuCtrl;
        this.events = events;
        this.navParams = navParams;
        this.menuCtrl.swipeEnable(false, 'mainMenu');
        this.events.subscribe("update", function () {
            _this.userProvider.userUpdatedDetails();
        });
        this.storage.get('isSignedIn').then(function (data) {
            if (data == true) {
                _this.storage.get("user").then(function (user) {
                    _this.userProvider.user = user;
                    _this.events.publish('usersignedin', user);
                    _this.navCtrl.setRoot(TabsPage);
                });
            }
        });
    }
    IndexPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad IndexPage');
    };
    IndexPage.prototype.goToSignIn = function () {
        this.navCtrl.push(SignInPage);
    };
    IndexPage.prototype.goToSignUp = function () {
        this.navCtrl.push(SignUpPage);
    };
    IndexPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-index',
            templateUrl: 'index.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            Storage,
            MenuController,
            Events,
            NavParams])
    ], IndexPage);
    return IndexPage;
}());
export { IndexPage };
//# sourceMappingURL=index.js.map