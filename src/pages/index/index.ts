import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Events, MenuController} from 'ionic-angular';
import {SignInPage} from "../sign-in/sign-in";
import {SignUpPage} from "../sign-up/sign-up";
import {Storage} from "@ionic/storage";
import {UserProvider} from "../../providers/user/user";
import {IUser} from "../../interfaces/IUser";
import {TabsPage} from "../tabs/tabs";
import {LandingPage} from "../landing/landing";

/**
 * Generated class for the IndexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-index',
  templateUrl: 'index.html',
})
export class IndexPage {

  constructor(public navCtrl: NavController,
              public userProvider : UserProvider,
              public storage : Storage,
              public menuCtrl: MenuController,
              public events: Events,
              public navParams: NavParams) {

      this.menuCtrl.swipeEnable(false, 'mainMenu');

      this.storage.get("showLanding").then((data)=>{
          if(data == true) this.navCtrl.setRoot(LandingPage);
      });

      this.events.subscribe("update",()=>{
         this.userProvider.userUpdatedDetails();
      });

      this.storage.get('isSignedIn').then((data:any)=>{
          if(data == true){
              this.storage.get("user").then((user:IUser)=>{
                  this.userProvider.user = user;
                  this.events.publish('usersignedin',user);
                  this.navCtrl.setRoot(TabsPage)
              })
          }
      });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad IndexPage');
  }

  goToSignIn(){
      this.navCtrl.push(SignInPage);
  }

  goToSignUp(){
      this.navCtrl.push(SignUpPage);
  }

}
