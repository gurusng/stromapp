var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProfilePage } from "../user-profile/user-profile";
import { UserProvider } from "../../providers/user/user";
/**
 * Generated class for the AdDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdDetailsPage = /** @class */ (function () {
    function AdDetailsPage(navCtrl, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.sorting = "created_at";
        this.currentPage = 1;
        this.type = 'approval';
        this.taskData = [
            {
                title: "Facebook Afiliate Task",
                body: "We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand.",
                max_no_of_users: "75",
                completed_no_of_users: "150",
                expiry_date: "150",
                overall_no_of_users: "150",
                type: "social",
                code: "hdsdsd",
                user: { name: "james" }
            }
        ];
        this.loading = false;
        this.loadingMore = false;
        this.task = this.navParams.get('task');
    }
    AdDetailsPage.prototype.ionViewDidEnter = function () {
        this.updateTaskDetails();
    };
    AdDetailsPage.prototype.ionViewDidLoad = function () {
        this.updateTaskDetails();
        //console.log('ionViewDidLoad Ad Details');
    };
    AdDetailsPage.prototype.rejectUser = function (tuid) {
        var _this = this;
        var data = {
            'tuid': tuid
        };
        this.userProvider.rejectTask(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Success", "Rejected user.");
                _this.userProvider.userUpdatedDetails();
                _this.updateTaskDetails();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AdDetailsPage.prototype.approveUser = function (tuid) {
        var _this = this;
        var data = {
            'tuid': tuid
        };
        this.userProvider.approveTask(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Success", "Approved user.");
                _this.userProvider.userUpdatedDetails();
                _this.updateTaskDetails();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AdDetailsPage.prototype.approveAll = function () {
        var _this = this;
        var data = {
            'tid': this.task.tid
        };
        this.userProvider.approveAllTasks(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Success", "Approved all users.");
                _this.userProvider.userUpdatedDetails();
                _this.updateTaskDetails();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AdDetailsPage.prototype.verifyAll = function () {
        var _this = this;
        var data = {
            'tid': this.task.tid
        };
        this.userProvider.verifyAllTasks(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Success", "Verified all users.");
                _this.userProvider.userUpdatedDetails();
                _this.updateTaskDetails();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AdDetailsPage.prototype.verifyUser = function (tuid) {
        var _this = this;
        var data = {
            'tuid': tuid
        };
        this.userProvider.verifyTask(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Success", "Verified user.");
                _this.userProvider.userUpdatedDetails();
                _this.updateTaskDetails();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AdDetailsPage.prototype.updateType = function (type) {
        this.type = type;
        this.updateTaskDetails();
    };
    AdDetailsPage.prototype.updateTaskDetails = function () {
        if (this.type == 'approval') {
            this.updateTaskDetailsApproval();
            return;
        }
        if (this.type == 'verification') {
            this.updateTaskDetailsVerification();
            return;
        }
    };
    AdDetailsPage.prototype.updateTaskDetailsApproval = function () {
        var _this = this;
        var data = {
            'tid': this.task.tid,
            'page': this.currentPage,
            'uid': this.userProvider.user.uid
        };
        this.loading = true;
        this.userProvider.userAdsDetailsApproval(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.task = success.data;
                if (success.data.users.data.length > 0) {
                    _this.currentPage = success.data.users.data.current_page;
                    success.data.users.data.forEach(function (data) {
                        //console.log(JSON.stringify(data));
                        _this.taskData.push(data);
                    });
                }
                //console.log(JSON.stringify(success.data));
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AdDetailsPage.prototype.updateTaskDetailsVerification = function () {
        var _this = this;
        var data = {
            'tid': this.task.tid,
            'page': this.currentPage,
            'uid': this.userProvider.user.uid
        };
        this.loading = true;
        this.userProvider.userAdsDetailsVerification(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.task = success.data;
                if (success.data.users.data.length > 0) {
                    _this.currentPage = success.data.users.data.current_page;
                    success.data.users.data.forEach(function (data) {
                        //console.log(JSON.stringify(data));
                        _this.taskData.push(data);
                    });
                }
                //console.log(JSON.stringify(success.data));
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AdDetailsPage.prototype.userDetail = function () {
        this.navCtrl.push(UserProfilePage);
    };
    AdDetailsPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    AdDetailsPage.prototype.loadMore = function (infiniteScroll) {
        //console.log('Begin async operation');
        var _this = this;
        var data = {
            'tid': this.task.tid,
            'type': this.task.type,
            'sort': this.sorting,
            'page': this.currentPage + 1
        };
        this.loadingMore = true;
        this.userProvider.userAdsDetails(data).subscribe(function (success) {
            _this.loadingMore = false;
            if (success.message == "Successful") {
                if (success.data.users.data.length > 0) {
                    _this.currentPage = success.data.users.data.current_page;
                    success.data.users.data.forEach(function (data) {
                        //console.log(JSON.stringify(data));
                        _this.taskData.push(data);
                    });
                }
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
            infiniteScroll.complete();
        }, function (error) {
            _this.loadingMore = false;
            //console.log(JSON.stringify(error));
            infiniteScroll.complete();
        });
    };
    AdDetailsPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-ad-details',
            templateUrl: 'ad-details.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            NavParams])
    ], AdDetailsPage);
    return AdDetailsPage;
}());
export { AdDetailsPage };
//# sourceMappingURL=ad-details.js.map