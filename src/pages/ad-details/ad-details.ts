import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserProfilePage} from "../user-profile/user-profile";
import {UserProvider} from "../../providers/user/user";

/**
 * Generated class for the AdDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ad-details',
  templateUrl: 'ad-details.html',
})
export class AdDetailsPage {

    sorting: string = "created_at";
    currentPage:number = 1;
    type:string = 'approval';

    taskData = [
        {
            title: "Facebook Afiliate Task",
            body: "We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand.",
            max_no_of_users: "75",
            completed_no_of_users: "150",
            expiry_date: "150",
            overall_no_of_users: "150",
            type: "social",
            code: "hdsdsd",
            user: {name: "james"}
        }
    ]

    task:any;
    loading:boolean = false;
    loadingMore:boolean = false;

    constructor(public navCtrl: NavController,
                public userProvider: UserProvider,
                public navParams: NavParams) {

        this.task = this.navParams.get('task');

    }


    ionViewDidEnter(){
        this.updateTaskDetails();
    }
    ionViewDidLoad() {
        this.updateTaskDetails();
        //console.log('ionViewDidLoad Ad Details');
    }

    rejectUser(tuid){
        let data  = {
            'tuid' : tuid
        };

        this.userProvider.rejectTask(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert( "Success","Rejected user.");
                this.userProvider.userUpdatedDetails();
                this.updateTaskDetails();
            } else{
                this.userProvider.showAlert("Error",success.message);
            }


        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }


    approveUser(tuid){
        let data  = {
            'tuid' : tuid
        };

        this.userProvider.approveTask(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert("Success","Approved user.");
                this.userProvider.userUpdatedDetails();
                this.updateTaskDetails();
            } else{
                this.userProvider.showAlert("Error",success.message);
            }


        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    approveAll(){

        let data  = {
            'tid' : this.task.tid
        };

        this.userProvider.approveAllTasks(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert("Success", "Approved all users.");
                this.userProvider.userUpdatedDetails();
                this.updateTaskDetails();
            } else{
                this.userProvider.showAlert("Error",success.message);
            }


        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    verifyAll(){


        let data  = {
            'tid' : this.task.tid
        };

        this.userProvider.verifyAllTasks(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert( "Success", "Verified all users.");
                this.userProvider.userUpdatedDetails();
                this.updateTaskDetails();
            } else{
                this.userProvider.showAlert("Error",success.message);
            }


        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    verifyUser(tuid){
        let data  = {
            'tuid' : tuid
        };

        this.userProvider.verifyTask(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert( "Success","Verified user.");
                this.userProvider.userUpdatedDetails();
                this.updateTaskDetails();

            } else{
                this.userProvider.showAlert("Error",success.message);
            }


        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }



    updateType(type){
        this.type = type;
        this.updateTaskDetails();
    }


    updateTaskDetails(){

        if (this.type == 'approval') {
            this.updateTaskDetailsApproval();
            return;
        }
        if (this.type == 'verification') {
            this.updateTaskDetailsVerification();
            return;
        }

    }

    updateTaskDetailsApproval(){
        let data = {
            'tid' : this.task.tid,
            'page' : this.currentPage,
            'uid' : this.userProvider.user.uid
        };

        this.loading = true;
        this.userProvider.userAdsDetailsApproval(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.task = success.data;

                if(success.data.users.data.length > 0 ) {

                    this.currentPage = success.data.users.data.current_page;
                    success.data.users.data.forEach((data)=>{

                        //console.log(JSON.stringify(data));
                        this.taskData.push(data);

                    });

                }

                //console.log(JSON.stringify(success.data));
            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    updateTaskDetailsVerification(){
        let data = {
            'tid' : this.task.tid,
            'page' : this.currentPage,
            'uid' : this.userProvider.user.uid
        };

        this.loading = true;
        this.userProvider.userAdsDetailsVerification(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.task = success.data;

                if(success.data.users.data.length > 0 ) {

                    this.currentPage = success.data.users.data.current_page;
                    success.data.users.data.forEach((data)=>{

                        //console.log(JSON.stringify(data));
                        this.taskData.push(data);

                    });

                }

                //console.log(JSON.stringify(success.data));
            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }


    userDetail() {
        this.navCtrl.push(UserProfilePage);
    }

    goBack() {
        this.navCtrl.pop();
    }


    loadMore(infiniteScroll) {
        //console.log('Begin async operation');

        let data = {
            'tid'  : this.task.tid,
            'type' : this.task.type,
            'sort' : this.sorting,
            'page' : this.currentPage + 1
        };

        this.loadingMore = true;
        this.userProvider.userAdsDetails(data).subscribe((success:any)=>{
            this.loadingMore = false;
            if(success.message == "Successful"){
                if(success.data.users.data.length > 0 ) {

                    this.currentPage = success.data.users.data.current_page;
                    success.data.users.data.forEach((data)=>{

                        //console.log(JSON.stringify(data));
                        this.taskData.push(data);

                    });

                }

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

            infiniteScroll.complete();

        },(error)=>{
            this.loadingMore = false;
            //console.log(JSON.stringify(error));
            infiniteScroll.complete();
        });


    }




}
