var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { App, Events, IonicPage, MenuController, NavController, NavParams, Platform } from 'ionic-angular';
import { UserProfilePage } from '../user-profile/user-profile';
import { UserProvider } from "../../providers/user/user";
import { TaskDescriptionPage } from '../task-description/task-description';
import { VideoDescriptionPage } from '../video-description/video-description';
import { CommunityDescriptionPage } from "../community-description/community-description";
import { FundPage } from "../fund/fund";
import { IndexPage } from "../index";
/**
 * Generated class for the OffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OffersPage = /** @class */ (function () {
    function OffersPage(navCtrl, platform, app, events, menuCtrl, userProvider, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.app = app;
        this.events = events;
        this.menuCtrl = menuCtrl;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.taskSorting = "created_at-desc";
        this.videoSorting = "created_at-desc";
        this.communitySorting = "created_at-desc";
        this.type = 'social';
        this.loading = false;
        this.currentPage = 1;
        this.display = 'ios-list-outline';
        this.taskData = [];
        this.communityData = [];
        this.videoData = [];
        this.section = 'social';
        platform.ready().then(function () {
            _this.menuCtrl.swipeEnable(true, 'mainMenu');
            try {
                window["plugins"].OneSignal.sendTag("user_id", _this.userProvider.user.uid);
                window["plugins"].OneSignal.sendTag("user_name", _this.userProvider.user.username);
            }
            catch (e) {
            }
        });
        this.events.subscribe("update", function (data) {
            _this.userProvider.userUpdatedDetails();
            var user = _this.userProvider.user;
        });
        this.events.subscribe("usersignedout", function () {
            _this.userProvider.logout();
            _this.app.getRootNav().setRoot(IndexPage);
        });
    }
    OffersPage.prototype.switchDisplay = function () {
        if (this.display == 'ios-list-outline')
            this.display = 'md-grid';
        else
            this.display = 'ios-list-outline';
    };
    OffersPage.prototype.ionViewDidEnter = function () {
        this.userProvider.userUpdatedDetails();
        this.getTasks();
    };
    OffersPage.prototype.userDetail = function () {
        this.app.getRootNav().setRoot(UserProfilePage);
    };
    OffersPage.prototype.getTasks = function () {
        var _this = this;
        var sorting;
        if (this.section == "social")
            sorting = this.taskSorting;
        if (this.section == "video")
            sorting = this.videoSorting;
        if (this.section == "community")
            sorting = this.communitySorting;
        var data = {
            'type': this.section,
            'sort': sorting
        };
        this.loading = true;
        this.userProvider.tasks(data).subscribe(function (success) {
            _this.loading = false;
            //console.log(success);
            if (success.message == "Successful") {
                _this.currentPage = success.data.current_page;
                if (success.data.data.length > 0) {
                    if (String(success.data.data[0].type).toLowerCase() == "social")
                        _this.taskData = success.data.data;
                    if (String(success.data.data[0].type).toLowerCase() == "community")
                        _this.communityData = success.data.data;
                    if (String(success.data.data[0].type).toLowerCase() == "videov2e" || String(success.data.data[0].type).toLowerCase() == "videop2v")
                        _this.videoData = success.data.data;
                }
                else {
                    _this.taskData = [];
                    _this.videoData = [];
                    _this.communityData = [];
                }
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    OffersPage.prototype.updateSection = function (section) {
        this.section = section;
        this.getTasks();
    };
    OffersPage.prototype.loadMore = function (infiniteScroll) {
        var _this = this;
        var sorting;
        if (this.section == "social")
            sorting = this.taskSorting;
        if (this.section == "video")
            sorting = this.videoSorting;
        if (this.section == "community")
            sorting = this.communitySorting;
        var data = {
            'type': this.section,
            'sort': sorting,
            'page': this.currentPage + 1
        };
        this.loading = true;
        this.userProvider.tasks(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                if (success.data.data.length > 0) {
                    _this.currentPage = success.data.current_page;
                    success.data.data.forEach(function (data) {
                        if (data.type == "social")
                            _this.taskData.push(data);
                        if (data.type == "community")
                            _this.communityData.push(data);
                        if (data.type == "videov2e" || data.type == "videop2v")
                            _this.videoData.push(data);
                    });
                }
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
            infiniteScroll.complete();
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error));
            infiniteScroll.complete();
        });
    };
    OffersPage.prototype.doRefresh = function (refresher) {
        //console.log('Begin async operation', refresher);
        this.getTasks();
        setTimeout(function () {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    OffersPage.prototype.goToDetails = function (task) {
        //console.log(task);
        this.navCtrl.push(TaskDescriptionPage, { 'task': task });
    };
    OffersPage.prototype.goToCommunityDetails = function (task) {
        //console.log(task);
        this.navCtrl.push(CommunityDescriptionPage, { 'task': task });
    };
    OffersPage.prototype.goToVideoDetails = function (video) {
        this.navCtrl.push(VideoDescriptionPage, { 'video': video });
    };
    OffersPage.prototype.goToFund = function () {
        this.navCtrl.push(FundPage);
    };
    OffersPage.prototype.searchVideos = function (event) {
        var _this = this;
        var term = event.target.value;
        this.loading = true;
        var data = {
            'term': term,
            'sort': this.videoSorting,
            'page': 1
        };
        this.userProvider.searchVideos(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.videoData = [];
                if (success.data.data.length > 0) {
                    _this.currentPage = success.data.current_page;
                    success.data.data.forEach(function (data) {
                        if (_this.section == "video")
                            _this.videoData.push(data);
                    });
                }
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
        //console.log(event);
    };
    OffersPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-offers',
            templateUrl: 'offers.html',
        }),
        __metadata("design:paramtypes", [NavController,
            Platform,
            App,
            Events,
            MenuController,
            UserProvider,
            NavParams])
    ], OffersPage);
    return OffersPage;
}());
export { OffersPage };
//# sourceMappingURL=offers.js.map