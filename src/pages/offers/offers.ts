import { Component } from '@angular/core';
import {App, Events, IonicPage, MenuController, NavController, NavParams, Platform} from 'ionic-angular';
import { UserProfilePage } from '../user-profile/user-profile';
import {UserProvider} from "../../providers/user/user";
import { TaskDescriptionPage } from '../task-description/task-description';
import { VideoDescriptionPage } from '../video-description/video-description';
import {CommunityDescriptionPage} from "../community-description/community-description";
import {FundPage} from "../fund/fund";
import {IndexPage} from "../index";
import {TabsPage} from "../tabs/tabs";
import {VerifyPage} from "../verify/verify";

/**
 * Generated class for the OffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offers',
  templateUrl: 'offers.html',
})
export class OffersPage {

    taskSorting: string = "created_at-desc";
    videoSorting: string = "created_at-desc";
    communitySorting: string = "created_at-desc";
    type:string = 'social';
    loading:boolean = false;
    currentPage:number = 1;
    refresher:any;

    display:string = 'ios-list-outline';
    taskData = [];

    communityData = [];

    videoData = [];

    section:string = 'social';

    constructor(  public navCtrl: NavController,
                  private platform: Platform,
                  private app :App,
                  public events : Events,
                  public menuCtrl: MenuController,
                  public userProvider : UserProvider,
                  public navParams: NavParams) {


        platform.ready().then(() => {
            this.menuCtrl.swipeEnable(true, 'mainMenu');

            try {
                window["plugins"].OneSignal.sendTag("user_id", this.userProvider.user.uid);
                window["plugins"].OneSignal.sendTag("user_name", this.userProvider.user.username);

            }catch (e) {

            }

        });


        this.events.subscribe("update",(data)=>{
            this.userProvider.userUpdatedDetails();
            let user = this.userProvider.user;
        });

        this.events.subscribe("usersignedout",()=>{
            this.userProvider.logout();
            this.app.getRootNav().setRoot(IndexPage);

        });

    }


    switchDisplay(){
        if(this.display == 'ios-list-outline') this.display = 'md-grid'; else this.display = 'ios-list-outline';
    }

    ionViewDidEnter() {
        this.userProvider.userUpdatedDetails();
        this.getTasks();
    }


    userDetail(){
        this.app.getRootNav().setRoot(UserProfilePage);
    }

    getTasks(){

        let sorting;

        if(this.section == "social") sorting = this.taskSorting;
        if(this.section == "video") sorting = this.videoSorting;
        if(this.section == "community") sorting = this.communitySorting;

      let data = {
          'type' : this.section,
          'sort' : sorting
      };

      this.loading = true;
      this.userProvider.tasks(data).subscribe((success:any)=>{
          this.loading = false;
          //console.log(success);
          if(success.message == "Successful"){

              this.currentPage = success.data.current_page;
              if(success.data.data.length > 0){
                  if(String(success.data.data[0].type).toLowerCase() == "social") this.taskData = success.data.data;
                  if(String(success.data.data[0].type).toLowerCase() == "community") this.communityData = success.data.data;
                  if(String(success.data.data[0].type).toLowerCase() == "videov2e" || String(success.data.data[0].type).toLowerCase() == "videop2v") this.videoData = success.data.data;
              }

              else{
                  this.taskData = [];
                  this.videoData = [];
                  this.communityData = [];
              }


          } else{
              this.userProvider.showAlert("Error",success.message);
          }

      },(error)=>{
          this.loading = false;
          //console.log(JSON.stringify(error))
      });


    }

    updateSection(section){
        this.section = section;
        this.getTasks();
    }


    loadMore(infiniteScroll) {

        let sorting;

        if(this.section == "social") sorting = this.taskSorting;
        if(this.section == "video") sorting = this.videoSorting;
        if(this.section == "community") sorting = this.communitySorting;


        let data = {
            'type' : this.section,
            'sort' : sorting,
            'page' : this.currentPage + 1
        };

        this.loading = true;
        this.userProvider.tasks(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                if(success.data.data.length > 0 ) {

                    this.currentPage = success.data.current_page;
                    success.data.data.forEach((data)=>{
                        if (data.type == "social") this.taskData.push(data);
                        if (data.type == "community") this.communityData.push(data);
                        if (data.type == "videov2e" || data.type == "videop2v") this.videoData.push(data);
                    });

                }

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

            infiniteScroll.complete();

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error));
            infiniteScroll.complete();
        });


    }


    doRefresh(refresher) {
        //console.log('Begin async operation', refresher);

        this.getTasks();
        setTimeout(() => {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }

    goToDetails(task) {
        //console.log(task);
        this.navCtrl.push(TaskDescriptionPage,{'task' : task});
    }

    goToCommunityDetails(task) {
        //console.log(task);
        this.navCtrl.push(CommunityDescriptionPage,{'task' : task});
    }

    goToVideoDetails(video) {
        this.navCtrl.push(VideoDescriptionPage,{'video' : video});
    }

    goToFund(){
        this.navCtrl.push(FundPage);
    }

    searchVideos(event){
        const term = event.target.value;
        this.loading = true;

        let data = {
            'term' : term,
            'sort' : this.videoSorting,
            'page' : 1
        };

        this.userProvider.searchVideos(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){

                this.videoData = [];

                if(success.data.data.length > 0 ) {

                    this.currentPage = success.data.current_page;
                    success.data.data.forEach((data)=>{
                       if (this.section == "video") this.videoData.push(data);
                    });

                }


            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

        //console.log(event);
    }

}
