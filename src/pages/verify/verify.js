var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
import { UploadPhotoPage } from "../upload-photo/upload-photo";
import { TabsPage } from "../tabs/tabs";
/**
 * Generated class for the VerifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VerifyPage = /** @class */ (function () {
    function VerifyPage(navCtrl, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.digit1 = "";
        this.digit2 = "";
        this.digit3 = "";
        this.digit4 = "";
        this.digit5 = "";
        this.digit6 = "";
        this.loading = false;
        this.interval = null;
    }
    VerifyPage.prototype.ionViewDidLoad = function () {
    };
    VerifyPage.prototype.ionViewDidEnter = function () {
        this.check();
    };
    VerifyPage.prototype.check = function () {
        this.userProvider.userUpdatedDetails();
        if (this.userProvider.user.isEmailVerified == "1") {
            this.navCtrl.setRoot(TabsPage);
        }
    };
    VerifyPage.prototype.move1 = function () {
        if (this.digit1 != "")
            this.digit2input.setFocus();
    };
    VerifyPage.prototype.move2 = function () {
        if (this.digit2 == "")
            this.digit1input.setFocus();
        else
            this.digit3input.setFocus();
    };
    VerifyPage.prototype.move3 = function () {
        if (this.digit3 == "")
            this.digit2input.setFocus();
        else
            this.digit4input.setFocus();
    };
    VerifyPage.prototype.move4 = function () {
        if (this.digit4 == "")
            this.digit3input.setFocus();
        else
            this.digit5input.setFocus();
    };
    VerifyPage.prototype.move5 = function () {
        if (this.digit5 == "")
            this.digit4input.setFocus();
        else
            this.digit6input.setFocus();
    };
    VerifyPage.prototype.move6 = function () {
        if (this.digit6 == "")
            this.digit5input.setFocus();
        if (this.digit6 != "")
            this.digit6[0];
        this.verify();
    };
    VerifyPage.prototype.resendCode = function () {
        var _this = this;
        var data = {
            'uid': this.userProvider.user.uid
        };
        this.loading = true;
        this.userProvider.resendCode(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Success", "Code has be sent again. Please check your phone and email.");
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            //console.log(JSON.stringify(error));
            _this.loading = false;
            _this.userProvider.showAlert("Error", "Network Issue. Please try again.");
        });
    };
    VerifyPage.prototype.verify = function () {
        var _this = this;
        this.code = this.digit1 + this.digit2 + this.digit3 + this.digit4 + this.digit5 + this.digit6;
        var data = {
            'uid': this.userProvider.user.uid,
            'code': this.code
        };
        this.loading = true;
        this.userProvider.verifyCode(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.navCtrl.push(UploadPhotoPage);
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            //console.log(JSON.stringify(error));
            _this.loading = false;
            _this.userProvider.showAlert("Error", "Network Issue. Please try again.");
        });
    };
    __decorate([
        ViewChild("digit1ref"),
        __metadata("design:type", Object)
    ], VerifyPage.prototype, "digit1input", void 0);
    __decorate([
        ViewChild("digit2ref"),
        __metadata("design:type", Object)
    ], VerifyPage.prototype, "digit2input", void 0);
    __decorate([
        ViewChild("digit3ref"),
        __metadata("design:type", Object)
    ], VerifyPage.prototype, "digit3input", void 0);
    __decorate([
        ViewChild("digit4ref"),
        __metadata("design:type", Object)
    ], VerifyPage.prototype, "digit4input", void 0);
    __decorate([
        ViewChild("digit5ref"),
        __metadata("design:type", Object)
    ], VerifyPage.prototype, "digit5input", void 0);
    __decorate([
        ViewChild("digit6ref"),
        __metadata("design:type", Object)
    ], VerifyPage.prototype, "digit6input", void 0);
    VerifyPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-verify',
            templateUrl: 'verify.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            NavParams])
    ], VerifyPage);
    return VerifyPage;
}());
export { VerifyPage };
//# sourceMappingURL=verify.js.map