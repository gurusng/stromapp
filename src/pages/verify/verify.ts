import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {UploadPhotoPage} from "../upload-photo/upload-photo";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the VerifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify',
  templateUrl: 'verify.html',
})
export class VerifyPage {

    loading:boolean = false;


    constructor(public navCtrl: NavController,
                public userProvider : UserProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
    }

    ionViewDidEnter(){
        this.check();
    }

    check(){
        this.userProvider.userUpdatedDetails();
        if(this.userProvider.user.isEmailVerified == "1") {

            this.navCtrl.setRoot(TabsPage);
        }
    }


    resendCode(){
        let data = {
            'uid' : this.userProvider.user.uid
        };

        this.loading = true;

        this.userProvider.resendCode(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert("Success", "Code has be sent again. Please check your phone and email.");

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            //console.log(JSON.stringify(error));
            this.loading = false;
            this.userProvider.showAlert("Error","Network Issue. Please try again.");

        });

    }


}
