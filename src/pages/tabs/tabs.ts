import {Component, ViewChild} from '@angular/core';

import {OffersPage} from "../offers/offers";
import {AdvertisePage} from "../advertise/advertise";
import {InvitePage} from "../invite/invite";
import {CashoutPage} from '../cashout/cashout';
import {NavController, NavParams, Tabs} from "ionic-angular";
import {VerifyPage} from "../verify/verify";
import {UserProvider} from "../../providers/user/user";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

    @ViewChild('myTabs') tabRef: Tabs;

    tab1Root = OffersPage;
    tab2Root = AdvertisePage;
    tab3Root = InvitePage;
    tab4Root = CashoutPage;


    constructor(
    public navParams: NavParams,
    public userProvider : UserProvider,
    public navCtrl: NavController,

    ) {}


    ionViewDidLoad(){

        // take the user to verify page if the account isn't verified
        if(this.userProvider.user.isEmailVerified == "1" || this.userProvider.user.isPhoneVerified == "1") {} else
            this.navCtrl.setRoot(VerifyPage);

        if(this.navParams.get('page') == 'advertise') this.tabRef.select(1);

    }




}
