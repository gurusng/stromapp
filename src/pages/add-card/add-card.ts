import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import { UserProfilePage } from '../user-profile/user-profile';
import {UserProvider} from "../../providers/user/user";

/**
 * Generated class for the AddCardage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-card',
  templateUrl: 'add-card.html',
})
export class AddCardPage {

    loading:boolean = false;
    cardNumber:string = "";
    cvv:string = "";
    expiryMonth:string = "";
    expiryYear:string = "";
    pin:string = "";
    // cardNumber:string = "5399838383838381";
    // cvv:string = "470";
    // expiryMonth:string = "10";
    // expiryYear:string = "22";
    // pin:string = "3310";
    reference:string;
    paymentMessage:string;

    showOtp = false;
    otp = "";

    constructor(public navCtrl: NavController,
                private app: App,
                public userProvider: UserProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad AddCardPage');
    }


    goToProfile() {
        this.app.getRootNav().setRoot(UserProfilePage);
    }

    goBack(){
        this.navCtrl.pop();
    }

    addCard(){
        let data = {
            'uid'        : this.userProvider.user.uid,
            'cardNumber' : this.cardNumber,
            'cvv'        : this.cvv,
            'expiryMonth': this.expiryMonth,
            'expiryYear' : this.expiryYear,
            'pin'        : this.pin
        };

        this.loading = true;
        this.userProvider.addCard(data).subscribe((success:any)=>{
            this.loading = false;
            //console.log(JSON.stringify(success));

            if(success.message == "Successful"){
                //console.log(success);
                this.reference = success.data.reference;
                this.paymentMessage = success.data.message;
                this.showOtp = true;
            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    authorizePayment(){
        let data = {
            'reference' : this.reference,
            'otp'       : this.otp
        };

        this.loading = true;

        this.userProvider.authorizePayment(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert("Successful","Card Added");
                this.userProvider.userUpdatedDetails();
                this.goToProfile()

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }


}
