var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProfilePage } from '../user-profile/user-profile';
import { UserProvider } from "../../providers/user/user";
/**
 * Generated class for the AddCardage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddCardPage = /** @class */ (function () {
    function AddCardPage(navCtrl, app, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.loading = false;
        this.cardNumber = "";
        this.cvv = "";
        this.expiryMonth = "";
        this.expiryYear = "";
        this.pin = "";
        this.showOtp = false;
        this.otp = "";
    }
    AddCardPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad AddCardPage');
    };
    AddCardPage.prototype.goToProfile = function () {
        this.app.getRootNav().setRoot(UserProfilePage);
    };
    AddCardPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    AddCardPage.prototype.addCard = function () {
        var _this = this;
        var data = {
            'uid': this.userProvider.user.uid,
            'cardNumber': this.cardNumber,
            'cvv': this.cvv,
            'expiryMonth': this.expiryMonth,
            'expiryYear': this.expiryYear,
            'pin': this.pin
        };
        this.loading = true;
        this.userProvider.addCard(data).subscribe(function (success) {
            _this.loading = false;
            //console.log(JSON.stringify(success));
            if (success.message == "Successful") {
                //console.log(success);
                _this.reference = success.data.reference;
                _this.paymentMessage = success.data.message;
                _this.showOtp = true;
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AddCardPage.prototype.authorizePayment = function () {
        var _this = this;
        var data = {
            'reference': this.reference,
            'otp': this.otp
        };
        this.loading = true;
        this.userProvider.authorizePayment(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Successful", "Card Added");
                _this.userProvider.userUpdatedDetails();
                _this.goToProfile();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    AddCardPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-add-card',
            templateUrl: 'add-card.html',
        }),
        __metadata("design:paramtypes", [NavController,
            App,
            UserProvider,
            NavParams])
    ], AddCardPage);
    return AddCardPage;
}());
export { AddCardPage };
//# sourceMappingURL=add-card.js.map