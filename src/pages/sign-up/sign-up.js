var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
import { VerifyPage } from "../verify/verify";
import { SignInPage } from "../sign-in/sign-in";
/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SignUpPage = /** @class */ (function () {
    function SignUpPage(navCtrl, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.loading = false;
    }
    SignUpPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad SignUpPage');
    };
    SignUpPage.prototype.signUp = function () {
        var _this = this;
        var user = {
            'username': this.username,
            'name': this.name,
            'phone': this.phone,
            'email': this.email,
            'password': this.password,
            'referredBy': this.referredBy
        };
        this.loading = true;
        this.userProvider.signUp(user).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.userProvider.showAlert("Success", "You have been signed up.");
                _this.userProvider.user = success.data;
                _this.navCtrl.setRoot(VerifyPage);
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            _this.userProvider.showAlert("Error", error.message);
            //console.log(JSON.stringify(error))
        });
        // this.navCtrl.push(VerifyPage);
    };
    SignUpPage.prototype.goToSignIn = function () {
        this.navCtrl.push(SignInPage);
    };
    SignUpPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-sign-up',
            templateUrl: 'sign-up.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            NavParams])
    ], SignUpPage);
    return SignUpPage;
}());
export { SignUpPage };
//# sourceMappingURL=sign-up.js.map