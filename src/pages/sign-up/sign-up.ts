import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {VerifyPage} from "../verify/verify";
import {SignInPage} from "../sign-in/sign-in";

/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

    loading:boolean = false;
    username:string;
    name:string;
    phone:string;
    referredBy:string;
    email:string;
    password:string;

    constructor(public navCtrl: NavController,
                public userProvider : UserProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad SignUpPage');
    }

    signUp(){
        let user = {
            'username'  : this.username,
            'name'  : this.name,
            'phone' : this.phone,
            'email' : this.email,
            'password' : this.password,
            'referredBy' : this.referredBy
        };

        this.loading = true;
        this.userProvider.signUp(user).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){

                this.userProvider.showAlert("Success","You have been signed up.");
                this.userProvider.user = success.data;

                this.navCtrl.setRoot(VerifyPage);

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            this.userProvider.showAlert("Error",error.message);
            //console.log(JSON.stringify(error))
        });

        // this.navCtrl.push(VerifyPage);

    }

    goToSignIn(){
        this.navCtrl.push(SignInPage);
    }

}
