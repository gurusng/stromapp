import {Component} from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-edit-profile',
    templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

    username:string;
    name:string;
    phone:string;
    email:string;
    loading:boolean = false;


    constructor(public navCtrl: NavController,
                private app: App,
                public userProvider: UserProvider,
                public navParams: NavParams) {
        this.username = this.userProvider.user.username;
        this.name = this.userProvider.user.name;
        this.email = this.userProvider.user.email;
        this.phone = this.userProvider.user.phone;

    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad EditProfilePage');
    }

    goBack() {
        this.navCtrl.pop();
    }

    editProfile() {
        let data = {
            'uid': this.userProvider.user.uid,
            'username' : this.username,
            'name' : this.name,
            'phone' : this.phone,
            'email' : this.email
        };

        this.loading = true;
        this.userProvider.editProfile(data).subscribe((success: any) => {
            this.loading = false;
            //console.log(JSON.stringify(success));

            if (success.message == "Successful") {

                this.userProvider.showToast("Profile Updated");
                this.userProvider.userUpdatedDetails();

            } else {
                this.userProvider.showAlert("Error", success.message);
            }

        }, (error) => {
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }


}
