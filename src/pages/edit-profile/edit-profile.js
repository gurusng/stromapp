var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditProfilePage = /** @class */ (function () {
    function EditProfilePage(navCtrl, app, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.loading = false;
        this.username = this.userProvider.user.username;
        this.name = this.userProvider.user.name;
        this.email = this.userProvider.user.email;
        this.phone = this.userProvider.user.phone;
    }
    EditProfilePage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad EditProfilePage');
    };
    EditProfilePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    EditProfilePage.prototype.editProfile = function () {
        var _this = this;
        var data = {
            'uid': this.userProvider.user.uid,
            'username': this.username,
            'name': this.name,
            'phone': this.phone,
            'email': this.email
        };
        this.loading = true;
        this.userProvider.editProfile(data).subscribe(function (success) {
            _this.loading = false;
            //console.log(JSON.stringify(success));
            if (success.message == "Successful") {
                _this.userProvider.showToast("Profile Updated");
                _this.userProvider.userUpdatedDetails();
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    EditProfilePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-edit-profile',
            templateUrl: 'edit-profile.html',
        }),
        __metadata("design:paramtypes", [NavController,
            App,
            UserProvider,
            NavParams])
    ], EditProfilePage);
    return EditProfilePage;
}());
export { EditProfilePage };
//# sourceMappingURL=edit-profile.js.map