import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommunityDescriptionPage } from './community-description';

@NgModule({
  declarations: [
    CommunityDescriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(CommunityDescriptionPage),
  ],
})
export class CommunityDescriptionPageModule {}
