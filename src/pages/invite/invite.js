var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProfilePage } from "../user-profile/user-profile";
import { UserProvider } from "../../providers/user/user";
import { FundPage } from '../fund/fund';
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { SocialSharing } from "@ionic-native/social-sharing";
/**
 * Generated class for the InvitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InvitePage = /** @class */ (function () {
    function InvitePage(navCtrl, userProvider, socialSharing, iab, app, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.socialSharing = socialSharing;
        this.iab = iab;
        this.app = app;
        this.navParams = navParams;
        this.section = 'inviteFriends';
        this.url = "";
    }
    InvitePage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad InvitePage');
    };
    InvitePage.prototype.userDetail = function () {
        this.app.getRootNav().setRoot(UserProfilePage);
    };
    InvitePage.prototype.fundPage = function () {
        this.navCtrl.push(FundPage);
    };
    InvitePage.prototype.updateSection = function (section) {
        this.section = section;
    };
    InvitePage.prototype.share = function () {
        this.url = "https://api.whatsapp.com/send?text=Download STROM app and earn rewards/points by completing social media/online tasks, watch or post videos to earn points, also patronise or refer friends to products / services to earn more points. Convert your rewards to real money and Cash out your earnings to your bank or get Airtime. Use my referral code " + this.userProvider.user.refCode + ". Find out more at https://play.google.com/store/apps/details?id=com.stromnetwork.mobileapps";
        this.navigate();
    };
    InvitePage.prototype.socialShare = function () {
        this.socialSharing.share("Download STROM app and earn rewards/points by completing social media/online tasks, watch or post videos to earn points, also patronise or refer friends to products / services to earn more points. Convert your rewards to real money and Cash out your earnings to your bank or get Airtime. Use my referral code " + this.userProvider.user.refCode + ". Find out more at https://play.google.com/store/apps/details?id=com.stromnetwork.mobileapps", "You are invited to join STROM Network", "https://play.google.com/store/apps/details?id=com.stromnetwork.mobileapps", "https://play.google.com/store/apps/details?id=com.stromnetwork.mobileapps");
    };
    InvitePage.prototype.navigate = function () {
        this.iab.create(this.url, "_system", "location=no,hidden=yes");
    };
    InvitePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-invite',
            templateUrl: 'invite.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            SocialSharing,
            InAppBrowser,
            App,
            NavParams])
    ], InvitePage);
    return InvitePage;
}());
export { InvitePage };
//# sourceMappingURL=invite.js.map