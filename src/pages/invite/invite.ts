import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserProfilePage} from "../user-profile/user-profile";
import {UserProvider} from "../../providers/user/user";
import { FundPage } from '../fund/fund';
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {SocialSharing} from "@ionic-native/social-sharing";


/**
 * Generated class for the InvitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invite',
  templateUrl: 'invite.html',
})
export class InvitePage {

    section:string = 'inviteFriends';
    url:string = "";

    constructor(public navCtrl: NavController,
                public userProvider: UserProvider,
                private socialSharing: SocialSharing,
                private iab: InAppBrowser,
                private app : App,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad InvitePage');
    }

    userDetail(){
        this.app.getRootNav().setRoot(UserProfilePage);
    }

    fundPage() {
        this.navCtrl.push(FundPage);
    }

    updateSection(section){
        this.section = section;
    }

    share(){
        this.url = "https://api.whatsapp.com/send?text=Download STROM app and earn rewards/points by completing social media/online tasks, watch or post videos to earn points, also patronise or refer friends to products / services to earn more points. Convert your rewards to real money and Cash out your earnings to your bank or get Airtime. Use my referral code " + this.userProvider.user.refCode + ". Find out more at https://play.google.com/store/apps/details?id=com.stromnetwork.mobileapps";
        this.navigate();

    }

    socialShare(){
        this.socialSharing.share("Download STROM app and earn rewards/points by completing social media/online tasks, watch or post videos to earn points, also patronise or refer friends to products / services to earn more points. Convert your rewards to real money and Cash out your earnings to your bank or get Airtime. Use my referral code " + this.userProvider.user.refCode + ". Find out more at https://play.google.com/store/apps/details?id=com.stromnetwork.mobileapps","You are invited to join STROM Network", "https://play.google.com/store/apps/details?id=com.stromnetwork.mobileapps", "https://play.google.com/store/apps/details?id=com.stromnetwork.mobileapps");
    }

    navigate(){
        this.iab.create(this.url,"_system","location=no,hidden=yes");

    }


}
