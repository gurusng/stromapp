import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoAdDetailsPage } from './video-ad-details';

@NgModule({
  declarations: [
    VideoAdDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoAdDetailsPage),
  ],
})
export class VideoAdDetailsPageModule {}
