var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
/**
 * Generated class for the VideoAdDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VideoAdDetailsPage = /** @class */ (function () {
    function VideoAdDetailsPage(navCtrl, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.loading = false;
        this.showVideo = false;
        this.video = this.navParams.get('video');
        this.updateTaskDetails();
    }
    VideoAdDetailsPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad VideoAdDetailsPage');
    };
    VideoAdDetailsPage.prototype.ionViewDidEnter = function () {
    };
    VideoAdDetailsPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    VideoAdDetailsPage.prototype.playVideo = function () {
        this.showVideo = true;
        this.videoElement = this.videop.nativeElement;
        this.videoElement.play();
    };
    VideoAdDetailsPage.prototype.updateTaskDetails = function () {
        var _this = this;
        var data = {
            'tid': this.video.tid,
            'uid': this.userProvider.user.uid
        };
        this.loading = true;
        this.userProvider.taskDetails(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.video = success.data;
                //console.log(JSON.stringify(success));
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    __decorate([
        ViewChild("videop"),
        __metadata("design:type", ElementRef)
    ], VideoAdDetailsPage.prototype, "videop", void 0);
    VideoAdDetailsPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-video-ad-details',
            templateUrl: 'video-ad-details.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            NavParams])
    ], VideoAdDetailsPage);
    return VideoAdDetailsPage;
}());
export { VideoAdDetailsPage };
//# sourceMappingURL=video-ad-details.js.map