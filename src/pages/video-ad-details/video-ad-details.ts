import {Component, ElementRef, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";

/**
 * Generated class for the VideoAdDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-video-ad-details',
  templateUrl: 'video-ad-details.html',
})
export class VideoAdDetailsPage {

    @ViewChild("videop") videop: ElementRef; // binds to #video in video.html
    videoElement: HTMLVideoElement;

    loading:boolean = false;
    video:any;
    showVideo:boolean = false;


    constructor(public navCtrl: NavController,
              public userProvider: UserProvider,
              public navParams: NavParams) {
      this.video = this.navParams.get('video');
        this.updateTaskDetails();

    }

    ionViewDidLoad() {
    //console.log('ionViewDidLoad VideoAdDetailsPage');
    }

    ionViewDidEnter(){
    }

    goBack() {
        this.navCtrl.pop();
    }

    playVideo(){
        this.showVideo = true;
        this.videoElement = this.videop.nativeElement;
        this.videoElement.play();


    }

    updateTaskDetails(){
        let data = {
            'tid' : this.video.tid,
            'uid' : this.userProvider.user.uid
        };

        this.loading = true;
        this.userProvider.taskDetails(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.video = success.data;

                //console.log(JSON.stringify(success));
            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

}
