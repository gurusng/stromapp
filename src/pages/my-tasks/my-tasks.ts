import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {TaskDescriptionPage} from "../task-description/task-description";
import {UserProvider} from "../../providers/user/user";
import {UserProfilePage} from "../user-profile/user-profile";
import {VideoDescriptionPage} from "../video-description/video-description";

/**
 * Generated class for the MyTasksPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-tasks',
  templateUrl: 'my-tasks.html',
})
export class MyTasksPage {


    taskSorting: string = "created_at-desc";
    videoSorting: string = "created_at-desc";
    communitySorting: string = "created_at-desc";

    loading:boolean = false;
    currentPage:number = 1;
    refresher:any;

    taskData = [];
    communityData = [];
    videoData = [];

    section:string = 'social';

    constructor(public navCtrl: NavController,
                private app :App,
                public userProvider : UserProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        this.getTasks();
    }

    ionViewDidEnter(){
        this.getTasks();
    }

    userDetail(){
        this.app.getRootNav().setRoot(UserProfilePage);
    }

    getTasks(){
        let sorting;

        if(this.section == "social") sorting = this.taskSorting;
        if(this.section == "video") sorting = this.videoSorting;
        if(this.section == "community") sorting = this.communitySorting;

        let data = {
            "uid" : this.userProvider.user.uid,
            "type" : this.section,
            "sort" : sorting
        };

        this.loading = true;
        this.userProvider.userTasks(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.currentPage = success.data.current_page;
                if(success.data.data.length > 0){
                    if(String(success.data.data[0].type).toLowerCase() == "social") this.taskData = success.data.data;
                    if(String(success.data.data[0].type).toLowerCase() == "community") this.communityData = success.data.data;
                    if(String(success.data.data[0].type).toLowerCase() == "videov2e" || String(success.data.data[0].type).toLowerCase() == "videop2v") this.videoData = success.data.data;
                }

                else{
                    this.taskData = [];
                    this.videoData = [];
                    this.communityData = [];
                }


            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });


    }

    updateSection(section){
        this.section = section;
        this.getTasks();
    }


    loadMore(infiniteScroll) {
        let sorting;

        if(this.section == "social") sorting = this.taskSorting;
        if(this.section == "video") sorting = this.videoSorting;
        if(this.section == "community") sorting = this.communitySorting;

        let data = {
            'uid'  : this.userProvider.user.uid,
            'type' : this.section,
            'sort' : sorting,
            'page' : this.currentPage + 1
        };

        this.loading = true;
        this.userProvider.userTasks(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                if(success.data.data.length > 0 ) {

                    this.currentPage = success.data.current_page;
                    success.data.data.forEach((data)=>{
                        if (data.type == "social") this.taskData.push(data);
                        if (data.type == "community") this.communityData.push(data);
                        if (data.type == "videov2e" || data.type == "videop2v") this.videoData.push(data);
                    });

                }

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

            infiniteScroll.complete();

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error));
            infiniteScroll.complete();
        });


    }


    doRefresh(refresher) {
        //console.log('Begin async operation', refresher);

        this.getTasks();
        setTimeout(() => {
            refresher.complete();
        }, 2000);
    }

    goToDetails(task) {

        this.navCtrl.push(TaskDescriptionPage,{'task' : task});
    }

    goToVideoDetails(video) {
        this.navCtrl.push(VideoDescriptionPage,{'video' : video});
    }

}
