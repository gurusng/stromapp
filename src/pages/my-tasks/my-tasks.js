var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaskDescriptionPage } from "../task-description/task-description";
import { UserProvider } from "../../providers/user/user";
import { UserProfilePage } from "../user-profile/user-profile";
import { VideoDescriptionPage } from "../video-description/video-description";
/**
 * Generated class for the MyTasksPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MyTasksPage = /** @class */ (function () {
    function MyTasksPage(navCtrl, app, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.taskSorting = "created_at-desc";
        this.videoSorting = "created_at-desc";
        this.communitySorting = "created_at-desc";
        this.loading = false;
        this.currentPage = 1;
        this.taskData = [];
        this.communityData = [];
        this.videoData = [];
        this.section = 'social';
    }
    MyTasksPage.prototype.ionViewDidLoad = function () {
        this.getTasks();
    };
    MyTasksPage.prototype.ionViewDidEnter = function () {
        this.getTasks();
    };
    MyTasksPage.prototype.userDetail = function () {
        this.app.getRootNav().setRoot(UserProfilePage);
    };
    MyTasksPage.prototype.getTasks = function () {
        var _this = this;
        var sorting;
        if (this.section == "social")
            sorting = this.taskSorting;
        if (this.section == "video")
            sorting = this.videoSorting;
        if (this.section == "community")
            sorting = this.communitySorting;
        var data = {
            "uid": this.userProvider.user.uid,
            "type": this.section,
            "sort": sorting
        };
        this.loading = true;
        this.userProvider.userTasks(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.currentPage = success.data.current_page;
                if (success.data.data.length > 0) {
                    if (String(success.data.data[0].type).toLowerCase() == "social")
                        _this.taskData = success.data.data;
                    if (String(success.data.data[0].type).toLowerCase() == "community")
                        _this.communityData = success.data.data;
                    if (String(success.data.data[0].type).toLowerCase() == "videov2e" || String(success.data.data[0].type).toLowerCase() == "videop2v")
                        _this.videoData = success.data.data;
                }
                else {
                    _this.taskData = [];
                    _this.videoData = [];
                    _this.communityData = [];
                }
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    MyTasksPage.prototype.updateSection = function (section) {
        this.section = section;
        this.getTasks();
    };
    MyTasksPage.prototype.loadMore = function (infiniteScroll) {
        var _this = this;
        var sorting;
        if (this.section == "social")
            sorting = this.taskSorting;
        if (this.section == "video")
            sorting = this.videoSorting;
        if (this.section == "community")
            sorting = this.communitySorting;
        var data = {
            'uid': this.userProvider.user.uid,
            'type': this.section,
            'sort': sorting,
            'page': this.currentPage + 1
        };
        this.loading = true;
        this.userProvider.userTasks(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                if (success.data.data.length > 0) {
                    _this.currentPage = success.data.current_page;
                    success.data.data.forEach(function (data) {
                        if (data.type == "social")
                            _this.taskData.push(data);
                        if (data.type == "community")
                            _this.communityData.push(data);
                        if (data.type == "videov2e" || data.type == "videop2v")
                            _this.videoData.push(data);
                    });
                }
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
            infiniteScroll.complete();
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error));
            infiniteScroll.complete();
        });
    };
    MyTasksPage.prototype.doRefresh = function (refresher) {
        //console.log('Begin async operation', refresher);
        this.getTasks();
        setTimeout(function () {
            refresher.complete();
        }, 2000);
    };
    MyTasksPage.prototype.goToDetails = function (task) {
        this.navCtrl.push(TaskDescriptionPage, { 'task': task });
    };
    MyTasksPage.prototype.goToVideoDetails = function (video) {
        this.navCtrl.push(VideoDescriptionPage, { 'video': video });
    };
    MyTasksPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-my-tasks',
            templateUrl: 'my-tasks.html',
        }),
        __metadata("design:paramtypes", [NavController,
            App,
            UserProvider,
            NavParams])
    ], MyTasksPage);
    return MyTasksPage;
}());
export { MyTasksPage };
//# sourceMappingURL=my-tasks.js.map