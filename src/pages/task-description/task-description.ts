import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
import { UserProfilePage } from '../user-profile/user-profile';
import {ITask} from "../../interfaces/ITask";

/**
 * Generated class for the TaskDescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-task-description',
    templateUrl: 'task-description.html',
})
export class TaskDescriptionPage {

    taskData = [
        {
            title: "Facebook Afiliate Task",
            body: "We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand.",
            max_no_of_users: "75",
            completed_no_of_users: "150",
            expiry_date: "150",
            overall_no_of_users: "150"
        }
    ]

    task:ITask;
    loading:boolean = false;

    constructor(public navCtrl: NavController,
        public userProvider: UserProvider,
        public navParams: NavParams) {

        this.task = this.navParams.get('task');

    }


    ionViewDidLoad() {
        this.updateTaskDetails();
        //console.log('ionViewDidLoad TaskDescriptionPage');
    }

    completeTask(){
        let data = {
            'tuid' : this.task.taskUser.tuid
        };

        this.loading = true;
        this.userProvider.completeTask(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){

                this.updateTaskDetails();
                //console.log(JSON.stringify(success));

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });


    }

    updateTaskDetails(){
        let data = {
          'tid' : this.task.tid,
          'uid' : this.userProvider.user.uid
        };

        this.loading = true;
        this.userProvider.taskDetails(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.task = success.data;

                //console.log(JSON.stringify(success));
            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    startTask(){
        let data = {
            'tid' : this.task.tid,
            'uid' : this.userProvider.user.uid
        };

        this.loading = true;
        this.userProvider.startTask(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){

                this.updateTaskDetails();
                //console.log(JSON.stringify(success));

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            //console.log(JSON.stringify(error))
        });

    }

    userDetail() {
        this.navCtrl.push(UserProfilePage);
    }

    goBack() {
        this.navCtrl.pop();
    }


}
