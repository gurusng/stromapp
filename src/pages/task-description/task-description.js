var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
import { UserProfilePage } from '../user-profile/user-profile';
/**
 * Generated class for the TaskDescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TaskDescriptionPage = /** @class */ (function () {
    function TaskDescriptionPage(navCtrl, userProvider, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.taskData = [
            {
                title: "Facebook Afiliate Task",
                body: "We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand. We are looking for people to advertsie the markeo brand.",
                max_no_of_users: "75",
                completed_no_of_users: "150",
                expiry_date: "150",
                overall_no_of_users: "150"
            }
        ];
        this.loading = false;
        this.task = this.navParams.get('task');
    }
    TaskDescriptionPage.prototype.ionViewDidLoad = function () {
        this.updateTaskDetails();
        //console.log('ionViewDidLoad TaskDescriptionPage');
    };
    TaskDescriptionPage.prototype.completeTask = function () {
        var _this = this;
        var data = {
            'tuid': this.task.taskUser.tuid
        };
        this.loading = true;
        this.userProvider.completeTask(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.updateTaskDetails();
                //console.log(JSON.stringify(success));
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    TaskDescriptionPage.prototype.updateTaskDetails = function () {
        var _this = this;
        var data = {
            'tid': this.task.tid,
            'uid': this.userProvider.user.uid
        };
        this.loading = true;
        this.userProvider.taskDetails(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.task = success.data;
                //console.log(JSON.stringify(success));
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    TaskDescriptionPage.prototype.startTask = function () {
        var _this = this;
        var data = {
            'tid': this.task.tid,
            'uid': this.userProvider.user.uid
        };
        this.loading = true;
        this.userProvider.startTask(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.updateTaskDetails();
                //console.log(JSON.stringify(success));
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            //console.log(JSON.stringify(error))
        });
    };
    TaskDescriptionPage.prototype.userDetail = function () {
        this.navCtrl.push(UserProfilePage);
    };
    TaskDescriptionPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    TaskDescriptionPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-task-description',
            templateUrl: 'task-description.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            NavParams])
    ], TaskDescriptionPage);
    return TaskDescriptionPage;
}());
export { TaskDescriptionPage };
//# sourceMappingURL=task-description.js.map