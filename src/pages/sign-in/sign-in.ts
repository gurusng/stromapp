import { Component } from '@angular/core';
import {Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {SignUpPage} from "../sign-up/sign-up";
import {UserProvider} from "../../providers/user/user";
import {TabsPage} from "../tabs/tabs";
import {Storage} from "@ionic/storage";
import {VerifyPage} from "../verify/verify";
import {ForgotPasswordPage} from "../forgot-password/forgot-password";

/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

    email:string ="";
    password:string = "";
    loading:boolean = false;
    showTerms:boolean = true;

    constructor(public navCtrl: NavController,
                public userProvider : UserProvider,
                public storage : Storage,
                public modalCtrl :ModalController,
                public events : Events,
                public navParams: NavParams) {

    }


    forgotPassword(){
        let modal = this.modalCtrl.create(ForgotPasswordPage);
        modal.present();
    }

    toggleTerms(){
        this.showTerms = !this.showTerms;
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad SignInPage');
    }

    goToSignUp(){
        this.navCtrl.push(SignUpPage);
    }

    signIn(){
        let data = {
            'email' : this.email,
            'password' : this.password
        };


        this.loading = true;

        this.userProvider.signIn(data).subscribe((success:any)=>{

            this.loading = false;
            if(success.message == "Successful"){


                this.storage.set('isSignedIn',true);
                this.storage.set('user',success.data);
                this.userProvider.user = success.data;


                this.events.publish("usersignedin",success.data);
                //console.log("signin event sent ");
                if(this.userProvider.user.isEmailVerified == "1" || this.userProvider.user.isPhoneVerified == "1"){
                    this.navCtrl.setRoot(TabsPage);
                } else
                    this.navCtrl.setRoot(VerifyPage);

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            this.userProvider.showAlert("Error",error.message);
            //console.log(JSON.stringify(error))
        });


    }
}
