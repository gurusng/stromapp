var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, IonicPage, ModalController, NavController, NavParams } from 'ionic-angular';
import { SignUpPage } from "../sign-up/sign-up";
import { UserProvider } from "../../providers/user/user";
import { TabsPage } from "../tabs/tabs";
import { Storage } from "@ionic/storage";
import { VerifyPage } from "../verify/verify";
import { ForgotPasswordPage } from "../forgot-password/forgot-password";
/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SignInPage = /** @class */ (function () {
    function SignInPage(navCtrl, userProvider, storage, modalCtrl, events, navParams) {
        this.navCtrl = navCtrl;
        this.userProvider = userProvider;
        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.navParams = navParams;
        this.email = "";
        this.password = "";
        this.loading = false;
        this.showTerms = true;
    }
    SignInPage.prototype.forgotPassword = function () {
        var modal = this.modalCtrl.create(ForgotPasswordPage);
        modal.present();
    };
    SignInPage.prototype.toggleTerms = function () {
        this.showTerms = !this.showTerms;
    };
    SignInPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad SignInPage');
    };
    SignInPage.prototype.goToSignUp = function () {
        this.navCtrl.push(SignUpPage);
    };
    SignInPage.prototype.signIn = function () {
        var _this = this;
        var data = {
            'email': this.email,
            'password': this.password
        };
        this.loading = true;
        this.userProvider.signIn(data).subscribe(function (success) {
            _this.loading = false;
            if (success.message == "Successful") {
                _this.storage.set('isSignedIn', true);
                _this.storage.set('user', success.data);
                _this.userProvider.user = success.data;
                _this.events.publish("usersignedin", success.data);
                //console.log("signin event sent ");
                if (_this.userProvider.user.isEmailVerified == "1" || _this.userProvider.user.isPhoneVerified == "1") {
                    _this.navCtrl.setRoot(TabsPage);
                }
                else
                    _this.navCtrl.setRoot(VerifyPage);
            }
            else {
                _this.userProvider.showAlert("Error", success.message);
            }
        }, function (error) {
            _this.loading = false;
            _this.userProvider.showAlert("Error", error.message);
            //console.log(JSON.stringify(error))
        });
    };
    SignInPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-sign-in',
            templateUrl: 'sign-in.html',
        }),
        __metadata("design:paramtypes", [NavController,
            UserProvider,
            Storage,
            ModalController,
            Events,
            NavParams])
    ], SignInPage);
    return SignInPage;
}());
export { SignInPage };
//# sourceMappingURL=sign-in.js.map