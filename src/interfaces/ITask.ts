export class ITask{
    tid:number;
    name:string;
    shortDescription:string;
    longDescription:string;
    expiry:string;
    points:number;
    maxUsers:number;
    verificationDuration:number;
    type:string;
    status:string;
    isSuspended:number;
    created_at:string;
    updated_at:string;
    taskUser:any;

}
