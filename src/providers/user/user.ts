import { Injectable } from '@angular/core';
import {AlertController, Events, ToastController} from "ionic-angular";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {IUser} from "../../interfaces/IUser";
import {Storage} from "@ionic/storage";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {


    // public env:string  =  'local';
    // public env:string  =  'staging';
    public env:string  =  'production';
    public url;
    public user:IUser;
    public inlinePaymentUrl;
    public points: any = 0;
    constructor(public http: HttpClient,
                public storage : Storage,
                public transfer: FileTransfer,
                public toastCtrl: ToastController,
                public alertCtrl: AlertController,
                public events : Events
                ) {

        this.setup();
    }

    setup(){
        if(this.env == 'local') this.url = 'http://localhost:8000/api/';
        if(this.env == 'staging') this.url = 'https://staging.stromnetwork.com/api/';
        if(this.env == 'staging') this.inlinePaymentUrl = 'https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/quidpay-inline.js';
        if(this.env == 'production') this.url = 'https://stromnetwork.com/api/';
        if(this.env == 'production') this.inlinePaymentUrl = 'https://api.ravepay.co/flwv3-pug/getpaidx/api/quidpay-inline.js';

    }

    signUp(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "sign-up",body,{headers});
    }

    signIn(user) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(user);
        return this.http.post(this.url + "sign-in",body,{headers});
    }

    verifyCode(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "verify-code",body,{headers});
    }

    resendCode(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "resend-code",body,{headers});
    }

    editProfile(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "edit-profile",body,{headers});
    }

    changePassword(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "change-password",body,{headers});
    }

    searchVideos(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "search-videos",body,{headers});
    }

    tasks(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "tasks",body,{headers});
    }

    pendingTasks(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "pending-tasks",body,{headers});
    }

    taskDetails(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "task-details",body,{headers});
    }

    userTasks(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "user-tasks",body,{headers});
    }

    userAds(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "user-ads",body,{headers});
    }

    userAdsDetails(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "user-ad-details",body,{headers});
    }

    userAdsDetailsApproval(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "user-ad-details-approval",body,{headers});
    }

    userAdsDetailsVerification(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "user-ad-details-verification",body,{headers});
    }

    userUpdatedDetails() {
        let data = {
            'uid' : this.user.uid
        };

        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        this.http.post(this.url + "user-updated-details",body,{headers})
            .subscribe((success:any)=>{
                console.log(success.data);
                this.user = success.data;
                this.events.publish("profile-update",this.user);
                this.points = new Intl.NumberFormat('en-US').format(success.data.points)
                if(this.user.isBanned == '1') {
                    this.events.publish("usersignedout");
                }
            });
    }


    changeProfilePhoto(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "change-profile-photo",body,{headers});
    }

    resolveAccountName(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "resolve-account-name",body,{headers});
    }

    cashoutAirtime(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "cash-out-airtime",body,{headers});
    }

    cashoutTransfer(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "cash-out-transfer",body,{headers});
    }

    addCard(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "add-card",body,{headers});
    }

    deleteCard(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "delete-card",body,{headers});
    }

    authorizePayment(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "authorize-payment",body,{headers});
    }

    creditPayments(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "web-pay",body,{headers});
    }

    webPay(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "web-pay",body,{headers});
    }

    pay(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "pay",body,{headers});
    }

    myReferrals(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "my-referrals",body,{headers});
    }

    startTask(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "start-task",body,{headers});
    }

    completeTask(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "complete-task",body,{headers});
    }

    verifyTask(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "verify-task",body,{headers});
    }

    verifyAllTasks(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "verify-all-tasks",body,{headers});
    }

    approveTask(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "approve-task",body,{headers});
    }

    approveAllTasks(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "approve-all-tasks",body,{headers});
    }

    rejectTask(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "reject-task",body,{headers});
    }

    playVideo(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "play-video",body,{headers});
    }

    addRating(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "add-rating",body,{headers});
    }

    addSocialTask(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "add-social-task",body,{headers});
    }

    addVideoTask(data) {
        let headers = new HttpHeaders();
            headers.set('Accept', "multipart/form-data");
            headers.set("Content-Type", null);
            return this.http.post(this.url + "add-video-task",data,{headers});
    }

    addCommunityTask(data) {
        let headers = new HttpHeaders();
        headers.set('Accept', "multipart/form-data");
        headers.set("Content-Type", null);
        return this.http.post(this.url + "add-community-task",data,{headers});
    }

    startCommunityTaskAndroid(data:any,images:string[]){

        let options: FileUploadOptions = {
            fileKey: 'images[]',
            fileName: 'upload.jpg',
            mimeType: "multipart/form-data",
            params : data
        };

        let self = this;

        const fileTransfer: FileTransferObject = this.transfer.create();

        fileTransfer.upload(images[0], this.url + 'add-community-task', options)
            .then((success:any) => {

                let data = JSON.parse(success.response);
                console.log(JSON.stringify(data.message));
                console.log('data next');
                console.log(JSON.stringify(data.data));
                console.log(images);

                if(data.message == "Successful"){


                    if(images.length > 1){ // send remaining pics

                        let sentCount = 1;
                        for(let i = 1; i < images.length; i++){

                            let subOptions: FileUploadOptions = {
                                fileKey: 'image',
                                fileName: 'upload.jpg',
                                mimeType: "multipart/form-data",
                                params : {
                                    'uid' : self.user.uid,
                                    'tid' : data.data.tid,
                                }
                            };

                            fileTransfer.upload(images[i], this.url + 'add-community-task-image', subOptions)
                                .then((success:any) => {

                                    if(success.message == "Successful"){
                                        sentCount++;

                                        if(sentCount == images.length){
                                            this.events.publish("community-finish");
                                        }
                                    }

                                    if(sentCount != images.length && i == images.length){
                                        this.events.publish("community-finished-some");
                                    }


                                    console.log(success);
                                },(error)=>{console.log(error)});

                        }

                    }



                } else {

                    self.showAlert("Error",data.message);
                    this.events.publish("community-failed");
                }

            }, (error) => {
                this.events.publish("community-failed");
                // error
                console.log(JSON.stringify(error));
                this.showAlert("Error","Something went wrong. Our engineers have been notified. Please try again.");

            })


    }


    resetPassword(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "reset-password",body,{headers});
    }

    resetPasswordCode(data) {
        let headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        let body = JSON.stringify(data);
        return this.http.post(this.url + "reset-password-code",body,{headers});
    }


    logout(){
        this.storage.set('isSignedIn',false);
        this.storage.remove("user");
        this.user = null;
    }


    showToast(message) {
        const toast = this.toastCtrl.create({
            message: message,
            duration: 2000,
            position : "middle",
            cssClass : "strom-toast"
        });
        toast.present();
    }





    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }



}
