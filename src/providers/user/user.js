var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { AlertController, Events, ToastController } from "ionic-angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { FileTransfer } from "@ionic-native/file-transfer";
/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var UserProvider = /** @class */ (function () {
    function UserProvider(http, storage, transfer, toastCtrl, alertCtrl, events) {
        this.http = http;
        this.storage = storage;
        this.transfer = transfer;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.events = events;
        // public env:string  =  'local';
        this.env = 'staging';
        this.points = 0;
        this.setup();
    }
    UserProvider.prototype.setup = function () {
        if (this.env == 'local')
            this.url = 'http://localhost:8000/api/';
        if (this.env == 'staging')
            this.url = 'https://staging.stromnetwork.com/api/';
        if (this.env == 'production')
            this.url = 'https://stromnetwork.com/api/';
    };
    UserProvider.prototype.signUp = function (user) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(user);
        return this.http.post(this.url + "sign-up", body, { headers: headers });
    };
    UserProvider.prototype.signIn = function (user) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(user);
        return this.http.post(this.url + "sign-in", body, { headers: headers });
    };
    UserProvider.prototype.verifyCode = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "verify-code", body, { headers: headers });
    };
    UserProvider.prototype.resendCode = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "resend-code", body, { headers: headers });
    };
    UserProvider.prototype.editProfile = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "edit-profile", body, { headers: headers });
    };
    UserProvider.prototype.changePassword = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "change-password", body, { headers: headers });
    };
    UserProvider.prototype.searchVideos = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "search-videos", body, { headers: headers });
    };
    UserProvider.prototype.tasks = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "tasks", body, { headers: headers });
    };
    UserProvider.prototype.pendingTasks = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "pending-tasks", body, { headers: headers });
    };
    UserProvider.prototype.taskDetails = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "task-details", body, { headers: headers });
    };
    UserProvider.prototype.userTasks = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "user-tasks", body, { headers: headers });
    };
    UserProvider.prototype.userAds = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "user-ads", body, { headers: headers });
    };
    UserProvider.prototype.userAdsDetails = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "user-ad-details", body, { headers: headers });
    };
    UserProvider.prototype.userAdsDetailsApproval = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "user-ad-details-approval", body, { headers: headers });
    };
    UserProvider.prototype.userAdsDetailsVerification = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "user-ad-details-verification", body, { headers: headers });
    };
    UserProvider.prototype.userUpdatedDetails = function () {
        var _this = this;
        var data = {
            'uid': this.user.uid
        };
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        this.http.post(this.url + "user-updated-details", body, { headers: headers })
            .subscribe(function (success) {
            console.log(success.data);
            _this.user = success.data;
            _this.events.publish("profile-update", _this.user);
            _this.points = new Intl.NumberFormat('en-US').format(success.data.points);
            if (_this.user.isBanned == '1') {
                _this.events.publish("usersignedout");
            }
        });
    };
    UserProvider.prototype.changeProfilePhoto = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "change-profile-photo", body, { headers: headers });
    };
    UserProvider.prototype.resolveAccountName = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "resolve-account-name", body, { headers: headers });
    };
    UserProvider.prototype.cashoutAirtime = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "cash-out-airtime", body, { headers: headers });
    };
    UserProvider.prototype.cashoutTransfer = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "cash-out-transfer", body, { headers: headers });
    };
    UserProvider.prototype.addCard = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "add-card", body, { headers: headers });
    };
    UserProvider.prototype.deleteCard = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "delete-card", body, { headers: headers });
    };
    UserProvider.prototype.authorizePayment = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "authorize-payment", body, { headers: headers });
    };
    UserProvider.prototype.creditPayments = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "web-pay", body, { headers: headers });
    };
    UserProvider.prototype.webPay = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "web-pay", body, { headers: headers });
    };
    UserProvider.prototype.pay = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "pay", body, { headers: headers });
    };
    UserProvider.prototype.myReferrals = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "my-referrals", body, { headers: headers });
    };
    UserProvider.prototype.startTask = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "start-task", body, { headers: headers });
    };
    UserProvider.prototype.completeTask = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "complete-task", body, { headers: headers });
    };
    UserProvider.prototype.verifyTask = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "verify-task", body, { headers: headers });
    };
    UserProvider.prototype.verifyAllTasks = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "verify-all-tasks", body, { headers: headers });
    };
    UserProvider.prototype.approveTask = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "approve-task", body, { headers: headers });
    };
    UserProvider.prototype.approveAllTasks = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "approve-all-tasks", body, { headers: headers });
    };
    UserProvider.prototype.rejectTask = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "reject-task", body, { headers: headers });
    };
    UserProvider.prototype.playVideo = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "play-video", body, { headers: headers });
    };
    UserProvider.prototype.addRating = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "add-rating", body, { headers: headers });
    };
    UserProvider.prototype.addSocialTask = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "add-social-task", body, { headers: headers });
    };
    UserProvider.prototype.addVideoTask = function (data) {
        var headers = new HttpHeaders();
        headers.set('Accept', "multipart/form-data");
        headers.set("Content-Type", null);
        return this.http.post(this.url + "add-video-task", data, { headers: headers });
    };
    UserProvider.prototype.addCommunityTask = function (data) {
        var headers = new HttpHeaders();
        headers.set('Accept', "multipart/form-data");
        headers.set("Content-Type", null);
        return this.http.post(this.url + "add-community-task", data, { headers: headers });
    };
    UserProvider.prototype.startCommunityTaskAndroid = function (data, images) {
        var _this = this;
        var options = {
            fileKey: 'images[]',
            fileName: 'upload.jpg',
            mimeType: "multipart/form-data",
            params: data
        };
        var self = this;
        var fileTransfer = this.transfer.create();
        fileTransfer.upload(images[0], this.url + 'add-community-task', options)
            .then(function (success) {
            var data = JSON.parse(success.response);
            console.log(JSON.stringify(data.message));
            console.log('data next');
            console.log(JSON.stringify(data.data));
            console.log(images);
            if (data.message == "Successful") {
                if (images.length > 1) {
                    var sentCount_1 = 1;
                    var _loop_1 = function (i) {
                        var subOptions = {
                            fileKey: 'image',
                            fileName: 'upload.jpg',
                            mimeType: "multipart/form-data",
                            params: {
                                'uid': self.user.uid,
                                'tid': data.data.tid,
                            }
                        };
                        fileTransfer.upload(images[i], _this.url + 'add-community-task-image', subOptions)
                            .then(function (success) {
                            if (success.message == "Successful") {
                                sentCount_1++;
                                if (sentCount_1 == images.length) {
                                    _this.events.publish("community-finish");
                                }
                            }
                            if (sentCount_1 != images.length && i == images.length) {
                                _this.events.publish("community-finished-some");
                            }
                            console.log(success);
                        }, function (error) { console.log(error); });
                    };
                    for (var i = 1; i < images.length; i++) {
                        _loop_1(i);
                    }
                }
            }
            else {
                self.showAlert("Error", data.message);
                _this.events.publish("community-failed");
            }
        }, function (error) {
            _this.events.publish("community-failed");
            // error
            console.log(JSON.stringify(error));
            _this.showAlert("Error", "Something went wrong. Our engineers have been notified. Please try again.");
        });
    };
    UserProvider.prototype.resetPassword = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "reset-password", body, { headers: headers });
    };
    UserProvider.prototype.resetPasswordCode = function (data) {
        var headers = new HttpHeaders()
            .set("Content-Type", "application/json");
        var body = JSON.stringify(data);
        return this.http.post(this.url + "reset-password-code", body, { headers: headers });
    };
    UserProvider.prototype.logout = function () {
        this.storage.set('isSignedIn', false);
        this.storage.remove("user");
        this.user = null;
    };
    UserProvider.prototype.showToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: "middle",
            cssClass: "strom-toast"
        });
        toast.present();
    };
    UserProvider.prototype.showAlert = function (title, message) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    };
    UserProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient,
            Storage,
            FileTransfer,
            ToastController,
            AlertController,
            Events])
    ], UserProvider);
    return UserProvider;
}());
export { UserProvider };
//# sourceMappingURL=user.js.map