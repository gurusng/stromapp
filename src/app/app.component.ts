import {Component} from '@angular/core';
import {App, Platform, Events, AlertController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {TabsPage} from "../pages/tabs/tabs";
import {MyAdsPage} from "../pages/my-ads/my-ads";
import {MyTasksPage} from "../pages/my-tasks/my-tasks";
import {FundPage} from "../pages/fund/fund";
import {TermsPage} from "../pages/terms/terms";
import {HelpPage} from "../pages/help/help";
import {IndexPage} from "../pages/index";



@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage: any = IndexPage;
    user: any = {name: "", email: ""};
    public alertShown:boolean = false;


    constructor(public platform: Platform,
                statusBar: StatusBar,
                public alertCtrl : AlertController,
                public events: Events,
                private app: App,
                splashScreen: SplashScreen) {
        let self = this;
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();

            // OneSignal Code start:
            // Enable to debug issues:
            // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

            console.log(platform.is('browser'));

            try{
                var notificationOpenedCallback = function (jsonData) {
                    self.events.publish("update",{}); // update users profile
                };

                window["plugins"].OneSignal
                    .startInit("f10a8d41-0944-47c1-8974-09644cdfaf55", "817343786721")
                    .handleNotificationOpened(notificationOpenedCallback)
                    .endInit();

                // register app uid with onesignal's
                // register username with onesignal's
            } catch (e) {
                console.log(e);
            }

            // end one signal


            // enables exit confirmation
            platform.registerBackButtonAction(() => {
                if (this.alertShown==false) {
                    if(this.app.getActiveNav().canGoBack() == false){
                        this.presentConfirm();
                    } else {
                        this.app.getActiveNav().pop();
                    }
                }
            }, 0);



        });


        this.events.subscribe("profile-update", (user) => {
            this.user = user;
            console.log(user);
            console.log('profile-update');
        });

        this.events.subscribe("usersignedin", (user) => {
            this.user = user;
        });


    }



    ionViewDidLoad() {

        // this.events.subscribe("usersignedin", (user) => {
        //     this.user = JSON.parse(user);
        //     console.log(user);
        //     console.log('event recieved');
        // });

        // this.events.subscribe("usersignedout", (user) => {
        //
        // });
    }

    presentConfirm() {
        let alert = this.alertCtrl.create({
            title: 'Confirm Exit',
            message: 'Do you want Exit?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                        this.alertShown=false;
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        console.log('Yes clicked');
                        this.platform.exitApp();
                    }
                }
            ]
        });
        alert.present().then(()=>{
            this.alertShown=true;
        });
    }

    goToOffers() {
        this.app.getRootNav().setRoot(TabsPage);
    }

    goToAdvertise() {
        this.app.getRootNav().setRoot(TabsPage,{'page' : 'advertise'});
    }

    goToMyAds() {
        this.app.getRootNav().setRoot(MyAdsPage);
    }

    goToMyTasks() {
        this.app.getRootNav().setRoot(MyTasksPage);
    }

    goToFund() {
        this.app.getRootNav().setRoot(FundPage);
    }

    goToTerms() {
        this.app.getRootNav().setRoot(TermsPage);
    }

    goToHelp() {
        this.app.getRootNav().setRoot(HelpPage);
    }


    logout() {

        this.events.publish("usersignedout");
        console.log("logout event sent");
    }

}
