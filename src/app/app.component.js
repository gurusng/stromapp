var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { App, Platform, Events, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LandingPage } from "../pages/landing/landing";
import { TabsPage } from "../pages/tabs/tabs";
import { MyAdsPage } from "../pages/my-ads/my-ads";
import { MyTasksPage } from "../pages/my-tasks/my-tasks";
import { FundPage } from "../pages/fund/fund";
import { TermsPage } from "../pages/terms/terms";
import { HelpPage } from "../pages/help/help";
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, alertCtrl, events, app, splashScreen) {
        var _this = this;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.app = app;
        this.rootPage = LandingPage;
        this.user = { name: "", email: "" };
        this.alertShown = false;
        var self = this;
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            // OneSignal Code start:
            // Enable to debug issues:
            // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
            console.log(platform.is('browser'));
            try {
                var notificationOpenedCallback = function (jsonData) {
                    self.events.publish("update", {}); // update users profile
                };
                window["plugins"].OneSignal
                    .startInit("f10a8d41-0944-47c1-8974-09644cdfaf55", "817343786721")
                    .handleNotificationOpened(notificationOpenedCallback)
                    .endInit();
                // register app uid with onesignal's
                // register username with onesignal's
            }
            catch (e) {
                console.log(e);
            }
            // end one signal
            // enables exit confirmation
            platform.registerBackButtonAction(function () {
                if (_this.alertShown == false) {
                    if (_this.app.getActiveNav().canGoBack() == false) {
                        _this.presentConfirm();
                    }
                    else {
                        _this.app.getActiveNav().pop();
                    }
                }
            }, 0);
        });
        this.events.subscribe("profile-update", function (user) {
            _this.user = user;
            console.log(user);
            console.log('profile-update');
        });
        this.events.subscribe("usersignedin", function (user) {
            _this.user = user;
        });
    }
    MyApp.prototype.ionViewDidLoad = function () {
        // this.events.subscribe("usersignedin", (user) => {
        //     this.user = JSON.parse(user);
        //     console.log(user);
        //     console.log('event recieved');
        // });
        // this.events.subscribe("usersignedout", (user) => {
        //
        // });
    };
    MyApp.prototype.presentConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm Exit',
            message: 'Do you want Exit?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                        _this.alertShown = false;
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        console.log('Yes clicked');
                        _this.platform.exitApp();
                    }
                }
            ]
        });
        alert.present().then(function () {
            _this.alertShown = true;
        });
    };
    MyApp.prototype.goToOffers = function () {
        this.app.getRootNav().setRoot(TabsPage);
    };
    MyApp.prototype.goToAdvertise = function () {
        this.app.getRootNav().setRoot(TabsPage, { 'page': 'advertise' });
    };
    MyApp.prototype.goToMyAds = function () {
        this.app.getRootNav().setRoot(MyAdsPage);
    };
    MyApp.prototype.goToMyTasks = function () {
        this.app.getRootNav().setRoot(MyTasksPage);
    };
    MyApp.prototype.goToFund = function () {
        this.app.getRootNav().setRoot(FundPage);
    };
    MyApp.prototype.goToTerms = function () {
        this.app.getRootNav().setRoot(TermsPage);
    };
    MyApp.prototype.goToHelp = function () {
        this.app.getRootNav().setRoot(HelpPage);
    };
    MyApp.prototype.logout = function () {
        this.events.publish("usersignedout");
        console.log("logout event sent");
    };
    MyApp = __decorate([
        Component({
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform,
            StatusBar,
            AlertController,
            Events,
            App,
            SplashScreen])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map