import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import {IonicStorageModule} from "@ionic/storage";

import { TabsPage } from '../pages/tabs/tabs';
import {OffersPage} from "../pages/offers/offers";
import {AdvertisePage} from "../pages/advertise/advertise";
import {InvitePage} from "../pages/invite/invite";
import {CashoutPage} from '../pages/cashout/cashout';
import {FundPage} from '../pages/fund/fund';
import { UserProfilePage } from '../pages/user-profile/user-profile';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import {OffersPageModule} from "../pages/offers/offers.module";
import {AdvertisePageModule} from "../pages/advertise/advertise.module";
import {InvitePageModule} from "../pages/invite/invite.module";
import {CashoutPageModule} from "../pages/cashout/cashout.module";
import {FundPageModule} from "../pages/fund/fund.module";
import {LandingPageModule} from "../pages/landing/landing.module";
import {IndexPageModule} from "../pages/index/index.module";
import { UserProvider } from '../providers/user/user';
import {SignInPageModule} from "../pages/sign-in/sign-in.module";
import {SignUpPageModule} from "../pages/sign-up/sign-up.module";
import {HttpClientModule} from "@angular/common/http";
import {VerifyPageModule} from "../pages/verify/verify.module";
import {UploadPhotoPageModule} from "../pages/upload-photo/upload-photo.module";
import { UserProfilePageModule } from '../pages/user-profile/user-profile.module';
import { TaskDescriptionPageModule } from '../pages/task-description/task-description.module';
import { VideoDescriptionPageModule } from '../pages/video-description/video-description.module';
import { AddCardPageModule } from '../pages/add-card/add-card.module';
import {Camera} from "@ionic-native/camera";
import {ImagePicker} from "@ionic-native/image-picker";
import {FileTransfer} from "@ionic-native/file-transfer";
import {MyTasksPageModule} from "../pages/my-tasks/my-tasks.module";
import {MyAdsPageModule} from "../pages/my-ads/my-ads.module";
import {AdDetailsPageModule} from "../pages/ad-details/ad-details.module";
import {CommunityDescriptionPageModule} from "../pages/community-description/community-description.module";
import {IonicImageViewerModule} from "ionic-img-viewer";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {TermsPageModule} from "../pages/terms/terms.module";
import { SocialSharing } from '@ionic-native/social-sharing';
import {HelpPageModule} from "../pages/help/help.module";
import {VideoAdDetailsPageModule} from "../pages/video-ad-details/video-ad-details.module";
import {EditProfilePageModule} from "../pages/edit-profile/edit-profile.module";
import {ForgotPasswordPageModule} from "../pages/forgot-password/forgot-password.module";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {PrivacyPageModule} from "../pages/privacy/privacy.module";


@NgModule({
  declarations: [
      MyApp,
      TabsPage
  ],
  imports: [
      BrowserModule,
      HttpClientModule,
      OffersPageModule,
      AdvertisePageModule,
      InvitePageModule,
      CashoutPageModule,
      LandingPageModule,
      SignInPageModule,
      SignUpPageModule,
      IndexPageModule,
      UploadPhotoPageModule,
      VerifyPageModule,
      UserProfilePageModule,
      TaskDescriptionPageModule,
      VideoDescriptionPageModule,
      CommunityDescriptionPageModule,
      MyTasksPageModule,
      MyAdsPageModule,
      AdDetailsPageModule,
      VideoAdDetailsPageModule,
      AddCardPageModule,
      HelpPageModule,
      ForgotPasswordPageModule,
      PrivacyPageModule,
      EditProfilePageModule,
      IonicImageViewerModule,
      FundPageModule,
      TermsPageModule,
      IonicModule.forRoot(MyApp,{
          tabsHideOnSubPages :true,

      }),
      IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
      MyApp,
      OffersPage,
      AdvertisePage,
      InvitePage,
      CashoutPage,
      UserProfilePage,
      TabsPage,
      FundPage
  ],
  providers: [
      StatusBar,
      SplashScreen,
      SocialSharing,
      Camera,
      ScreenOrientation,
      ImagePicker,
      InAppBrowser,
      FileTransfer,
      {provide: ErrorHandler, useClass: IonicErrorHandler},
      UserProvider
  ]
})
export class AppModule {}
